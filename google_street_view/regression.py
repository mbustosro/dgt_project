#Regression

#get features positives with number of accidents
#get features negatives with 0 number of accidents
#create train and test set
#create weights
#make regression linear, svr or neural net

import numpy as np 
import time
import matplotlib.pyplot as plt
import itertools
from db_controller import DB_Controller

N = 640*640
traffic = False


def group_img_objects(objs):
	objs = objs[0]
	ide = objs[0] 
	road = objs[1] / N
	sidewalk = objs[2] / N
	building = (objs[3] + objs[4] + objs[5]) / N
	traffic_objects = (objs[6] + objs[7] + objs[8]) / N
	nature = (objs[9] + objs[10]) / N
	sky = objs[11] / N
	people = (objs[12] + objs[13]) / N
	vehicles = (objs[14] + objs[15] + objs[16] + objs[17] + objs[18] + objs[19]) / N

	return road, sidewalk, building, traffic_objects, nature, sky, people, vehicles

def get_all_feature_vectors(radio):
	db_control = DB_Controller()
	if traffic:
		positive_points = db_control.select_points_with_accidents_around_radio(radio)
		negatives_points = db_control.select_points_without_accidents_around_radio(radio)
	else:
		positive_points = db_control.select_points_with_accidents_around_radio_no_traffic(radio)
		negatives_points = db_control.select_points_without_accidents_around_radio_no_traffic(radio)

	road = sidewalk = building = traffic_objects = nature = sky = people = vehicles = 0
	road_n = sidewalk_n = building_n = traffic_objects_n = nature_n = sky_n = people_n = vehicles_n = 0

	features_positives = []
	features_negatives = []
	x = ''
	y = []

	idx = 0

	for p in positive_points:
		ide = p[0]
		accidents = p[1]
		# segmented objects respect image size
		img_objects= db_control.select_objects_image_by_id(ide)

		road, sidewalk, building, traffic_objects, nature, sky, people, vehicles = group_img_objects(img_objects)

		x = [road, sidewalk, building, traffic_objects, nature, sky, people, vehicles] 
		# segmented Objects neighbors
		img_objects_neighbors =  db_control.get_neighbors_objects_img(ide, radio)
		road_n, sidewalk_n, building_n, traffic_objects_n, nature_n, sky_n, people_n, vehicles_n = group_img_objects(img_objects_neighbors)

		x = x + [road_n, sidewalk_n, building_n, traffic_objects_n, nature_n, sky_n, people_n, vehicles_n] 

		#Visibility: relation between segmented objects and objects 3d
		#objs_vis_pct =  db_control.get_visibility_pcte(ide)
		objs_vis_pct =  db_control.select_objects_visibility_3d(ide)
		road_vis, sidewalk_vis = objs_vis_pct[0]

		x = x + [road_vis, sidewalk_vis]

		#Visibility neighbors:
		#neighbors_vis_pct =  db_control.get_visibility_neighbor_pcte(ide, radio)
		neighbors_vis_pct =  db_control.select_objects_neighbors_visibility_3d(ide, radio)
		neigh_road_vis, neigh_sidewalk_vis = neighbors_vis_pct[0]

		x = x + [neigh_road_vis, neigh_sidewalk_vis]

		# 3D objects respect image size
		objs_3d = db_control.select_objects_3d_size(ide)
		road_3d, sidewalk_3d = objs_3d[0]

		x = x + [road_3d, sidewalk_3d]


		if traffic:
			isTram = db_control.get_tram_by_point(ide)
			if len(isTram) == 0:
				tram = 0
			else:
				tram = isTram[0][1]
			x = x + [tram]


		y = y + [accidents]

		features_positives.append(x)
		if(idx%100 == 0):
			print(idx)
		idx += 1

	features_positives = np.asarray(features_positives)
	features_positives = features_positives.astype(float)
	y_positives = y

	np.save('features_positives_notraffic_'+str(radio)+'.npy',features_positives)
	np.save('y_positives_notraffic_'+str(radio)+'.npy',y_positives)

	for p in negatives_points:
		ide = p[0]
		# objects for the image
		img_objects= db_control.select_objects_image_by_id(ide)
		road, sidewalk, building, traffic_objects, nature, sky, people, vehicles = group_img_objects(img_objects)

		x = [road, sidewalk, building, traffic_objects, nature, sky, people, vehicles]
		# Objects neighbors
		img_objects_neighbors =  db_control.get_neighbors_objects_img(ide, radio)
		road_n, sidewalk_n, building_n, traffic_objects_n, nature_n, sky_n, people_n, vehicles_n = group_img_objects(img_objects_neighbors)

		x = x + [road_n, sidewalk_n, building_n, traffic_objects_n, nature_n, sky_n, people_n, vehicles_n]

		#Visibility: relation between segmented objects and objects 3d
		#objs_vis_pct =  db_control.get_visibility_pcte(ide)
		objs_vis_pct =  db_control.select_objects_visibility_3d(ide)
		road_vis, sidewalk_vis = objs_vis_pct[0]

		x = x + [road_vis, sidewalk_vis]

		#Visibility neighbors:
		#neighbors_vis_pct =  db_control.get_visibility_neighbor_pcte(ide, radio)
		neighbors_vis_pct =  db_control.select_objects_neighbors_visibility_3d(ide, radio)
		neigh_road_vis, neigh_sidewalk_vis = neighbors_vis_pct[0]
		
		x = x + [neigh_road_vis, neigh_sidewalk_vis]


		# 3D objects respect image size
		objs_3d = db_control.select_objects_3d_size(ide)
		road_3d, sidewalk_3d = objs_3d[0]

		x = x + [road_3d, sidewalk_3d]

		if traffic:
			isTram = db_control.get_tram_by_point(ide)
			if len(isTram) == 0:
				tram = 0
			else:
				tram = 1
			x = x + [tram]

		features_negatives.append(x)
		if(idx%100 == 0):
			print(idx)
		idx += 1

	features_negatives = np.asarray(features_negatives)
	features_negatives = features_negatives.astype(float)
	y_negatives = np.zeros(len(features_negatives))

	np.save('features_negatives_notraffic_'+str(radio)+'.npy',features_negatives)
	np.save('y_negatives_notraffic_'+str(radio)+'.npy',y_negatives)

	db_control.close_database()



######################################## Regression ##################################################

def plot_correlation_matrix(cm, x_labels, y_labels, filename = '',
                          title='correlation matrix between features',
                          size = (20,10),
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """

    print(cm)
    plt.figure(figsize=size)
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    plt.xticks(np.arange(len(x_labels)), x_labels, rotation=45, horizontalalignment="right")
    plt.yticks(np.arange(len(y_labels)), y_labels)

    fmt = '.2f'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    #plt.ylabel('True label')
    #plt.xlabel('Predicted label')
    plt.savefig(filename)
    plt.show()

def get_balanced_set(x, y, n_samples, maximum_y):

	x_balanced = []
	y_balanced = []
	
	for i in range(maximum_y):
		idx = np.where(y==i)[0]
		np.random.shuffle(idx)
		acum = 0
		for j in idx:
			x_balanced.append(x[j])
			y_balanced.append(y[j])
			acum += 1
			if(acum >= n_samples):
				break

	x_balanced = np.asarray(x_balanced)
	y_balanced = np.asarray(y_balanced)
	return x_balanced, y_balanced

def get_balanced_train_test_set(x, y, n_train, n_test, maximum_y):
	X_train = []
	y_train = []
	X_test  = []
	y_test  = []

	for i in range(maximum_y):
		idx = np.where(y==i)[0]
		#print('idx ',i,len(idx))
		np.random.shuffle(idx)
		acum_train = 0
		acum_test = 0
		for j in idx:
			if(acum_train < n_train):
				X_train.append(x[j])
				y_train.append(y[j])
				acum_train += 1
			else:
				if(acum_test < n_test):
					X_test.append(x[j])
					y_test.append(y[j])
					acum_test += 1

	X_train = np.asarray(X_train)
	y_train = np.asarray(y_train)
	X_test = np.asarray(X_test)
	y_test = np.asarray(y_test)
	return X_train, y_train, X_test, y_test
			

def max_min_scaler(y, max, min):
	
	y_scaled = ((y-min)/(max-min))
	return y_scaled


def correlation_between_features(x):
	from scipy.stats import pearsonr
	n = x.shape[1]
	corr_mat = np.zeros((n,n))
	for i in range(n):
		for j in range(n):
			r, p_value = pearsonr(x[:,i], x[:,j])
			corr_mat[i,j] =r

	return corr_mat

def correlation_features_output(x,y):
	from scipy.stats import pearsonr
	n = x.shape[1]
	corr_mat = np.zeros((n,1))
	for i in range(n):
		r, p_value = pearsonr(x[:,i], y)
		corr_mat[i] =r

	return corr_mat

'''
#radios = [100,150,200,250,300]
radios = [50]
for r in radios:
	get_all_feature_vectors(r)
'''


from sklearn.utils import shuffle
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error, r2_score

#data preparation
radio = 50
n_samples = 1500
feature_names = ["road", "sidewalk", "building", "traffic_objects", "nature", "sky", "people", "vehicles",
				"road_n", "sidewalk_n", "building_n", "traffic_objects_n", "nature_n", "sky_n", "people_n", "vehicles_n",
				"road_vis", "sidewalk_vis", "neigh_road_vis", "neigh_sidewalk_vis", "road_3d", "sidewalk_3d"]

if traffic: 
	feature_names = feature_names + "traffic_flow"

X_p = np.load('features/features_positives_notraffic_'+str(radio)+'.npy')
y_p = np.load('features/y_positives_notraffic_'+str(radio)+'.npy')

X_n = np.load('features/features_negatives_notraffic_'+str(radio)+'.npy')
y_n = np.load('features/y_negatives_notraffic_'+str(radio)+'.npy')

X_p = np.nan_to_num(X_p)
X_n = np.nan_to_num(X_n)

X_all = np.concatenate((X_p, X_n), axis = 0)
y_all = np.concatenate((y_p, y_n), axis = 0)


#X_all = X_all[:,16:]
#get batches for sizes
#balance classes
iter = 5
maximum = max(y_all)
minimum = min(y_all)
maximum_y = int(maximum)
#scaler = preprocessing.StandardScaler()
scaler = preprocessing.MinMaxScaler()
scaler.fit(X_all)

'''
corr_mat = correlation_between_features(X_all)
plot_correlation_matrix(corr_mat, feature_names, feature_names, filename='corr_mat_features.png')
corr_mat = correlation_features_output(X_all, y_all)
plot_correlation_matrix(corr_mat, ['accidents'], feature_names, filename='corr_mat_features_accidents.png', size=(8,10))
'''

for i in range(iter):

	X, y, X_test, y_test = get_balanced_train_test_set(X_all, y_all, n_samples, n_samples, maximum_y)
	#X, X_test, y, y_test = train_test_split(X_all, y_all, test_size=0.4)
	#Normalize data
	#escalar tambien para TEST!!!
	X = scaler.transform(X)
	X_test = scaler.transform(X_test)

	#y = max_min_scaler(y, maximum, minimum)
	#y_test = max_min_scaler(y_test, maximum, minimum)
	# Models
	lr = LinearRegression()
	lr.fit(X, y)

	y_pred_lr = lr.predict(X_test)

	svr = SVR(C=1.0, kernel='rbf', epsilon=0.2,max_iter=1000)
	svr.fit(X,y)
	y_pred_svr = svr.predict(X_test)

	mlp = MLPRegressor(hidden_layer_sizes=(3,),max_iter=500)
	mlp.fit(X, y)
	y_pred_mlp = mlp.predict(X_test)

	print(i)
	print(lr.coef_)
	print("Mean squared error: %.2f"
      % mean_squared_error(y_test, y_pred_lr))
	# Explained variance score: 1 is perfect prediction
	print('Variance score: %.2f' % r2_score(y_test, y_pred_lr))


	print("Mean squared error svr: %.2f"
      % mean_squared_error(y_test, y_pred_svr))
	# Explained variance score: 1 is perfect prediction
	print('Variance score svr: %.2f' % r2_score(y_test, y_pred_svr))

	print("Mean squared error mlp: %.2f"
      % mean_squared_error(y_test, y_pred_mlp))
	# Explained variance score: 1 is perfect prediction
	print('Variance score mlp: %.2f' % r2_score(y_test, y_pred_mlp))
	#yy = np.transpose(np.vstack((y_test,y_pred)))

	#for j in yy:
	#	print(j)

	