import psycopg2

class DB_connection:

	def __init__(self, dbname, user, host, password):
		args = "dbname='"+dbname+"' user='"+user+"' host='"+host+"' password='"+password+"'"
		try:
			self.conn = psycopg2.connect(args)
			self.cur = self.conn.cursor()
			#print('database connected')
		except:
			self.conn = None
			print('I am unable to connect to the database')
			

	def close_connection(self):
		self.cur.close()
		self.conn.close()
		#print('database closed')

	def isConnected(self):
		return self.conn is not None

	def execute_select(self,query):
		if self.isConnected():
			try:
				self.cur.execute(query)
				rows = self.cur.fetchall()
				return rows
			except psycopg2.Error as e:
				print("Can not execute the query: " + query + ' because '+ e.diag.message_primary)
				return []
		else:
			print("Database not connected")
			return []

	def execute_query(self, query):
		if self.isConnected():
			try:
				self.cur.execute(query)
				self.conn.commit() #make my changes persistent
				return True
			except psycopg2.Error as e:
				print("Can not execute the query: " + query + ' because '+ e.diag.message_primary)
				return False
			
		else:
			print("Database not connected")
			return False

	