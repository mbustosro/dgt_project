# Compare images
import cv2
import numpy as np
import time
import os
from db_controller import DB_Controller

WIDTH = 20000
input_folder = '/Volumes/ExternalDiskSegate/espacio_persona/Barcelona/'
seg_folder_path  = input_folder + 'seg_read_images/'
img_3d_folder_path = input_folder + '3d_images/' 

def get_folder_names(input_folder,total_points=600000):
  folder_names = []
  for i in np.arange(0,total_points,WIDTH):
    if(i%WIDTH==0):
      folder_name = str(i)+'-'+str(i+WIDTH)
      folder_path = os.path.join(input_folder,folder_name)
      folder_names.append(folder_path)
  return folder_names


'''
Img_3d values:
0 = 'road'
48 = 'building'
111 = 'sidewalk'
255 = 'sky'


Image segmentada values:
0 = 'road'
1 = 'sidewalk'
2 = 'building' 
3 = 'wall'
4 = 'fence' 
5 = 'pole'
6 = 'traffic light'
7 = 'traffic sign'
8 = 'vegetation' 
9 = 'terrain' 
10 = 'sky'
11 = 'person'
12 = 'rider' 
13 = 'car' 
14 = 'truck'
15 = 'bus'
16 = 'train'
17 = 'motorcycle'
18 = 'bicycle'
'''


folder_names_3d = get_folder_names(img_3d_folder_path)
folder_names_seg = get_folder_names(seg_folder_path)

db_control = DB_Controller()
points  = db_control.select_unique_points_with_image()

n = 640
values_3d = np.array((0,48,111,255))
temp = np.array((),dtype=int)

for point in points:

	point_idx = p
	point_idx_str = str(point_idx)
	folder_idx = int(point_idx/WIDTH)
	img_3d_path = folder_names_3d[folder_idx] + '/'+ str(point_idx) + '.png'
	img_seg_path = folder_names_seg[folder_idx] + '/'+ str(point_idx) + '.png'

	#img_3d_path = '573890.png'
	#img_seg_path = '573890_seg.png'  

	img_3d = cv2.imread(img_3d_path, cv2.IMREAD_GRAYSCALE)
	img_seg = cv2.imread(img_seg_path, cv2.IMREAD_GRAYSCALE)

	#Both images are same fix size
	min_idx = 0

	road_vis = 0
	building_vis = 0
	sidewalk_vis = 0
	sky_vis = 0


	for i in range(n):
		for j in range(n): 
			pxl_3d =  img_3d[i,j]
			pxl_seg = img_seg[i,j]

			if (pxl_3d != values_3d).all:
				temp = abs(pxl_3d - values_3d)
				min_idx = np.argmin(temp)
				img_3d[i,j] = values_3d[min_idx]
				pxl_3d =  img_3d[i,j]
			#road	
			if(pxl_3d==0 and pxl_seg==0):
				road_vis += 1
			#building
			elif(pxl_3d==48 and pxl_seg==2):
				building_vis += 1
			elif(pxl_3d==111 and pxl_seg==1):
				sidewalk_vis += 1
			elif(pxl_3d==255 and pxl_seg==10):
				sky_vis += 1

	purity_pct = (road_vis + building_vis + sidewalk_vis + sky_vis) * 100 / (n*n)  			

	'''
	#VERIFY IF ALL 3D IMAGES PIXELS ARE n^2
	img_3d_array = img_3d.flatten()
	y = np.bincount(img_3d_array)
	ii = np.nonzero(y)[0]
	freq = np.vstack((ii,y[ii])).T
	print(freq)
	'''

	print('road_vis',road_vis)
	print('building_vis',building_vis)
	print('sidewalk_vis',sidewalk_vis)
	print('sky_vis',sky_vis)
	print('purity', purity_pct)

	#db_control.insert_objects_visibility_3d(ide,road_vis,building_vis,sidewalk_vis,sky_vis,purity_pct)

db_control.close_database()









