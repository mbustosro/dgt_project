from GSV_Image import GSV_Image
from postgres_connector import DB_connection
import json
from utils import *

class DB_Controller:

	def __init__(self):
		self.dbname 	= 'espaiPersona' 
		#self.dbname 	= 'Dummy_dgt' 
		self.user		= 'postgres'
		self.host		= 'localhost'
		self.password	= '41392175xy'
		self.schema 	= 'streetview_madrid'
		#self.schema 	= 'dummy_schema'
		self.db_conn 	= DB_connection(self.dbname, self.user, self.host, self.password)

	def set_schema(self, schema):
		self.schema = schema

	#excute any query
	def exe_query(self, query):	
		self.db_conn.execute_query(query)

	def exe_select_query(self, query):	
		rows = self.db_conn.execute_select(query)
		return rows
		
########################################## SEARCH_PTS ######################################
	def insert_search_points(self, ide, lat, lng, heading, pitch):
		query = ("INSERT INTO " + self.schema + ".search_pts (id, lat, lng, heading, pitch) "
				  + " VALUES ("	+ ide + ","
						   		+ lat + ","
						   		+ lng + ","
								+ heading + ","
								+ pitch
								+ ");"	)
		#print(query)
		self.db_conn.execute_query(query)

	
	def select_search_points(self):
		query = ("SELECT * FROM "+self.schema + ".search_pts;")
		rows = self.db_conn.execute_select(query)
		#for row in rows:
		#    print(row)
		return rows

	def select_search_point(self, ide):
		query = ("SELECT * FROM "+self.schema + ".search_pts WHERE id="+str(ide)+";")
		rows = self.db_conn.execute_select(query)
		#for row in rows:
		#    print(row)
		return rows[0]	# im sure there at least one result

	def select_not_processed_points(self, limit):
		query = ("SELECT * FROM "+self.schema + ".search_pts WHERE processed_status='N' LIMIT "+str(limit)+";")
		rows = self.db_conn.execute_select(query)
		#print(query)
		#for row in rows:
		#    print(row)
		return rows

	def update_search_points_in_process(self, ide):

		query = ("UPDATE " + self.schema + ".search_pts SET "
				  + " processed_status = 'P'" 
				  + " WHERE id = " + str(ide) + ";" )
		#print(query)
		self.db_conn.execute_query(query)

	def update_search_points_response(self, ide, response):
		#process copyright with invalid caracters
		#error_example = Can not execute the query: UPDATE streetview_bcn.search_pts SET  processed_status = 'Y' , response = '{"copyright": "\u00a9 I'm Moving To Barcelona", 
		if(response['status']=='OK'):
			copyright = response['copyright']
			copyright = copyright[2:]
			if(copyright != 'Google, Inc.'):
				response['copyright'] = 'INVALID_COPYRIGHT'

		query = ("UPDATE " + self.schema + ".search_pts SET "
				  + " processed_status = 'Y' ,"
				  + " response = '" + json.dumps(response) + "'" 
				  + " WHERE id = " + str(ide) + ";" )
		#print(query)
		self.db_conn.execute_query(query)


########################################## RETURNED_PTS ######################################
	def insert_returned_points(self, ide):
		query = ("INSERT INTO "+ self.schema + ".returned_pts (id) "
				+ " VALUES ("	+ ide + ");")
		self.db_conn.execute_query(query)

	def update_returned_point_status(self, ide, status):

		query = ("UPDATE " + self.schema + ".returned_pts SET "
				   + " query_status='"+ status + "'"
				   + " WHERE id = "+ str(ide) + ";"	)
		#print(query)
		self.db_conn.execute_query(query)

	def select_points_ready_for_process(self):
		query = ("SELECT id FROM "+self.schema + ".returned_pts"
				  +	" WHERE query_status='READY';")
		rows = self.db_conn.execute_select(query)
		return rows

	def look_for_location_matches(self,lat,lng,heading):
		query = ("SELECT id FROM "+self.schema + ".returned_pts"
				  +	" WHERE lat="+ str(lat) 
				  +	" AND lng="+ str(lng) 
				  +	" AND (@(heading - "+str(heading)+") <= 10)"
				  +	" LIMIT 1;")

		#print(query)
		rows = self.db_conn.execute_select(query)
		return rows

	def update_unique_point(self, ide, response_status, lat, lng, heading, pitch):
		query = ("UPDATE " + self.schema + ".returned_pts SET"
				   		   +" query_status = '"	+ response_status + "',"
						   +" lat = "	+ str(lat) + ","
						   +" lng = "	+ str(lng) + ","
						   +" heading = "	+ str(heading) + ","
						   +" pitch = "	+ str(pitch) 
						   +" WHERE id = "+ str(ide) + ";"	)
		#print(query)
		self.db_conn.execute_query(query)

	def update_repeated_point(self, ide, response_status, repeated_id):
		query = ("UPDATE " + self.schema + ".returned_pts SET"
				   			+" query_status='"+ response_status + "',"
							+" repeated_id="+ str(repeated_id) 
							+ " WHERE id = "+ str(ide) + ";"	)
		#print(query)
		self.db_conn.execute_query(query)

	def select_unique_points_without_image(self, limit):
		query = ("SELECT  id, lat, lng, heading, pitch FROM " + self.schema + ".returned_pts WHERE "
				  + " query_status = 'UNIQUE'"
				  + " AND download_status = 'N'" 
				  + " LIMIT "+str(limit)+";")
		rows = self.db_conn.execute_select(query)
		#print(query)
		#for row in rows:
		#    print(row)
		return rows

	def update_image_info_in_process(self, ide):
		query = ("UPDATE " + self.schema + ".returned_pts SET "
				  + " download_status = 'P'"
				  + " WHERE id = " + str(ide) + ";" )
		#print(query)
		self.db_conn.execute_query(query)

	def update_image_info(self, ide, img_name):
		query = ("UPDATE " + self.schema + ".returned_pts SET "
				  + " download_status = 'Y' ,"
				  + " gsv_img = '" + img_name + "'" 
				  + " WHERE id = " + str(ide) + ";" )
		#print(query)
		self.db_conn.execute_query(query)

	def select_invalid_copyright_points(self):
		query = ("SELECT  s.id, s.lat, s.lng, s.heading, s.pitch FROM " + self.schema + ".search_pts as s "
				  + " INNER JOIN " + self.schema + ".returned_pts as r "
				  + " ON s.id = r.id "
			      + " WHERE query_status = 'INVALID_COPYRIGHT';")
		rows = self.db_conn.execute_select(query)
		#print(query)
		#for row in rows:
		#    print(row)
		return rows

	def select_unique_points_with_image(self):
		query = ("SELECT * FROM " + self.schema + ".returned_pts WHERE "
				  + " query_status = 'UNIQUE'"
				  + " AND download_status = 'Y';")
		rows = self.db_conn.execute_select(query)
		#print(query)
		#for row in rows:
		#    print(row)
		return rows

	def update_image_info_to_neg(self, ide):
		query = ("UPDATE " + self.schema + ".returned_pts SET "
				  + " download_status = 'N' ,"
				  + " gsv_img = NULL" 
				  + " WHERE id = " + str(ide) + ";" )
		#print(query)
		self.db_conn.execute_query(query)

	########################################## ACCIDENTS ######################################	
	def select_accidents_locations(self):
		query = ("SELECT exp_id, lat, lng FROM "+self.schema + ".accidents_location;")
		rows = self.db_conn.execute_select(query)
		#for row in rows:
		#    print(row)
		return rows

	def insert_accidents_locations(self, exp_id, year, utm_x, utm_y, lat, lng):
		query = ("INSERT INTO " + self.schema + ".accidents_location (exp_id, year, utm_x, utm_y, lat, lng) "
				  + " VALUES ('"	+ exp_id + "',"
						   		+ year + ","
						   		+ utm_x + ","
								+ utm_y + ","
								+ lat + ","
								+ lng
								+ ");"	)
		#print(query)
		self.db_conn.execute_query(query)

	def insert_accidents_people(self, exp_id, person_type, age, genre):
		query = ("INSERT INTO " + self.schema + ".accidents_people (exp_id, person_type, age, genre) "
				  + " VALUES ('"	+ exp_id + "',"
						   		+ "'" + person_type + "',"
						   		+ age + ","
								+ "'" + genre + "'" 
								+ ");"	)
		#print(query)
		self.db_conn.execute_query(query)

	########################################## ACCIDENTS BY POINT ######################################	
	def insert_closest_points(self, exp_id, ide, distance):
		query = ("INSERT INTO " + self.schema + ".closest_points (exp_id, id, distance) "
				  + " VALUES ('"	+ exp_id + "',"
						   		+ ide + ","
								+ distance
								+ ");"	)
		#print(query)
		self.db_conn.execute_query(query)

	def select_points_with_accidents(self):
		query = ("SELECT cp.id,count(*) FROM "+self.schema + ".closest_points AS cp INNER JOIN streetview_bcn.accidents_people "
				+ "AS ap ON cp.exp_id=ap.exp_id WHERE ap.person_type = 'Vianant' GROUP BY cp.id ORDER BY count DESC ;")
		rows = self.db_conn.execute_select(query)

		#print(query)
		#for row in rows:
		#    print(row)
		return rows

	def select_points_without_accidents(self):
		query = ("SELECT id FROM "+self.schema + ".returned_pts WHERE id NOT IN (SELECT cp.id FROM "+self.schema + ".closest_points "
				+" AS cp INNER JOIN streetview_bcn.accidents_people AS ap ON cp.exp_id=ap.exp_id WHERE ap.person_type = 'Vianant' "
				+" GROUP BY cp.id) AND query_status='UNIQUE';")
		rows = self.db_conn.execute_select(query)

		#print(query)
		#for row in rows:
		#    print(row)
		return rows


	def select_points_with_accidents_around_radio(self, radio):
		query = ("SELECT id,count(*) FROM "+self.schema + ".accidents_around_point_200m WHERE distance<"+str(radio)+" GROUP BY id")
		rows = self.db_conn.execute_select(query)
		return rows

	def select_points_without_accidents_around_radio(self, radio):
		query = (" (SELECT id FROM "+self.schema + ".returned_pts WHERE query_status='UNIQUE')"
				+" EXCEPT "
				+" (SELECT id FROM "+self.schema + ".accidents_around_point_200m WHERE distance<"+str(radio)+" GROUP BY id)")
		rows = self.db_conn.execute_select(query)
		return rows

	def number_accidents_by_radio(self, radio):
		query = ("SELECT number_acc,count(*) FROM"
				+" (SELECT id,count(*) as number_acc FROM streetview_bcn.accidents_around_point_200m" 
				+" WHERE distance<"+ str(radio)+" group by id) as foo"
				+" GROUP BY number_acc order by 1")
		rows = self.db_conn.execute_select(query)
		return rows

	#Without data traffic
	def select_points_with_accidents_around_radio_no_traffic(self, radio):
		query = ("SELECT a.id,count(*) FROM streetview_bcn.accidents_around_point_200m AS a"
				+" LEFT OUTER JOIN streetview_bcn.points_trams_openbcn AS p ON a.id = p.id"
				+" WHERE p.id IS NULL AND distance<"+ str(radio)+" GROUP BY a.id")
		rows = self.db_conn.execute_select(query)
		return rows

	def select_points_without_accidents_around_radio_no_traffic(self, radio):
		query = (" (SELECT r.id FROM streetview_bcn.returned_pts AS r "
				+" LEFT OUTER JOIN streetview_bcn.points_trams_openbcn AS p ON r.id = p.id"
				+" WHERE p.id IS NULL AND query_status='UNIQUE')"
				+" EXCEPT"
				+" (SELECT id FROM streetview_bcn.accidents_around_point_200m WHERE distance<"+ str(radio)+" GROUP BY id)")
		rows = self.db_conn.execute_select(query)
		return rows

	##########################################OBJECTS IN IMAGE ######################################
	def insert_objects_image(self, ide, road, sidewalk, building, wall, fence, pole, traffic_light, traffic_sign, 
								vegetation, terrain, sky, person, rider, car, truck, bus, train, motorcycle, bicycle):
		query = ("INSERT INTO " + self.schema + ".objects_image "
								+ "(id, road, sidewalk, building, wall, fence, pole, traffic_light, traffic_sign," 
								+ "vegetation, terrain, sky, person, rider, car, truck, bus, train, motorcycle, bicycle) "
				  + " VALUES ('"	+ str(ide) + "',"
							   		+ str(road) + ","
							   		+ str(sidewalk) + ","
									+ str(building) + ","
									+ str(wall) + ","
									+ str(fence) + ","
							   		+ str(pole) + ","
									+ str(traffic_light) + ","
									+ str(traffic_sign) + ","
									+ str(vegetation) + ","
									+ str(terrain) + ","
									+ str(sky) + ","
									+ str(person) + ","
							   		+ str(rider) + ","
									+ str(car) + ","
									+ str(truck) + ","
									+ str(bus) + ","
							   		+ str(train)+ ","
									+ str(motorcycle) + ","
									+ str(bicycle) 
									+ ");")
			#print(query)
		self.db_conn.execute_query(query)

	def select_objects_image_by_id(self, ide):
		query = ("SELECT * FROM "+self.schema + ".objects_image WHERE id = " + str(ide) + ";" )
		rows = self.db_conn.execute_select(query)
		#for row in rows:
		#    print(row)
		return rows

	def select_objects_image(self):
		query = ("SELECT * FROM "+self.schema + ".objects_image;" )
		rows = self.db_conn.execute_select(query)
		return rows


	def get_values_objects_visibility(self):
		query = ("SELECT o.id,o.road,o.sidewalk,"
				+ " o.road/NULLIF(o.sidewalk,0) as ratio, "
				+ "	ov.road as road_vis, "
				+ "	ov.sidewalk as sidewalk_vºis,"
				+ "	road_sidewalk, "
				+ "	ov.road/NULLIF(o.road,0) as road_pct, "
				+ "	road_sidewalk/NULLIF(o.sidewalk,0) as rs_sidewalk_pct"
				+ "	FROM " + self.schema + ".objects_3d o"
				+ "	INNER JOIN " + self.schema + ".objects_visibility_3d AS ov ON o.id = ov.id ORDER BY id")
		rows = self.db_conn.execute_select(query)
		return rows

	def get_neighbors_objects_img(self, ide, radio):
		query = ("SELECT AVG(id),AVG(road), AVG(sidewalk), AVG(building), AVG(wall), AVG(fence)," 
				+" AVG(pole), AVG(traffic_light), AVG(traffic_sign), "
				+" AVG(vegetation), AVG(terrain), AVG(sky), AVG(person), AVG(rider), AVG(car), "
				+" AVG(truck), AVG(bus), AVG(train), AVG(motorcycle), AVG(bicycle) FROM "
				+" (SELECT * FROM " + self.schema + ".objects_image WHERE id IN "
				+" (SELECT point_2 FROM " + self.schema + ".neighbor_points WHERE point_1="+str(ide)
				+" AND distance<"+str(radio)+")) AS foo")
		rows = self.db_conn.execute_select(query)
		return rows
	##########################################OBJECTS VISIBILITY 3D ######################################	
	def insert_objects_visibility_3d(self,ide,road,building,sidewalk,sky,road_sidewalk):
		query = ("INSERT INTO " + self.schema + ".objects_visibility_3d "
								+ "(id, road, building, sidewalk, sky, road_sidewalk) "
				  + " VALUES ("	+ str(ide) + ","
							   		+ str(road) + ","
							   		+ str(building) + ","
									+ str(sidewalk) + ","
									+ str(sky) + ","
									+ str(road_sidewalk) 
									+ ");")
		self.db_conn.execute_query(query)

	def select_objects_visibility_3d(self, ide):
		N = str(640*640)
		query = ("SELECT (road+road_sidewalk)/"+ N +", sidewalk/"+ N +" FROM "+self.schema + ".objects_visibility_3d WHERE id = " + str(ide) + ";" )
		rows = self.db_conn.execute_select(query)
		return rows

	def select_objects_neighbors_visibility_3d(self, ide, radio):
		N = str(640*640)
		query = ("SELECT AVG(road + road_sidewalk)/"+ N +", AVG(sidewalk)/"+ N 
				+" FROM " + self.schema + ".objects_visibility_3d as ov"
				+" WHERE ov.id IN"
				+" (SELECT point_2 FROM " + self.schema + ".neighbor_points WHERE point_1="+ str(ide)
				+" AND distance<"+str(radio)+") ")
		rows = self.db_conn.execute_select(query)
		return rows

	def get_visibility_pcte(self,ide):
		query = ("SELECT r_vis + rs_vis as t_vis, s_vis FROM"
				+"(SELECT o.id,COALESCE(ov.road/NULLIF(o.road,0),0) as r_vis, "
				+" COALESCE(ov.sidewalk/NULLIF(o.sidewalk,0),0) as s_vis," 
				+" COALESCE(ov.road_sidewalk/NULLIF(o.sidewalk,0),0) as rs_vis"
				+" FROM " + self.schema + ".objects_3d as o"
				+" INNER JOIN " + self.schema + ".objects_visibility_3d as ov"
				+" ON o.id=ov.id"
				+" WHERE o.id = "+ str(ide) +") as foo")
		rows = self.db_conn.execute_select(query)
		return rows

	def get_visibility_neighbor_pcte(self, ide, radio):
		query = ("SELECT AVG(road + road_sidewalk), AVG(sidewalk) FROM"
				+" (SELECT COALESCE(ov.road/NULLIF(o.road,0),0) as road, "
				+" COALESCE(ov.sidewalk/NULLIF(o.sidewalk,0),0) as sidewalk, "
				+" COALESCE(ov.road_sidewalk/NULLIF(o.sidewalk,0),0) as road_sidewalk"
				+" FROM " + self.schema + ".objects_3d as o"
				+" INNER JOIN " + self.schema + ".objects_visibility_3d as ov"
				+" ON o.id=ov.id"
				+" WHERE o.id IN"
				+" (SELECT point_2 FROM " + self.schema + ".neighbor_points WHERE point_1="+ str(ide)
				+" AND distance<"+str(radio)+")) as foo")
		rows = self.db_conn.execute_select(query)
		return rows

	########################################## OBJECTS 3D ######################################
	def insert_objects_3d(self,ide,road,building,sidewalk,sky):
		query = ("INSERT INTO " + self.schema + ".objects_3d "
								+ "(id, road, building, sidewalk, sky) "
				  + " VALUES ("	+ str(ide) + ","
							   		+ str(road) + ","
							   		+ str(building) + ","
									+ str(sidewalk) + ","
									+ str(sky) 
									+ ");")
		self.db_conn.execute_query(query)

	def select_objects_3d_size(self, ide):
		N = str(640*640)
		query = ("SELECT road/"+ N +", sidewalk/"+ N +" FROM "+self.schema + ".objects_3d WHERE id = " + str(ide) + ";" )
		rows = self.db_conn.execute_select(query)
		return rows	

	def select_objects_3d(self, ide):
		query = ("SELECT * FROM "+self.schema + ".objects_3d WHERE id = " + str(ide) + ";" )
		rows = self.db_conn.execute_select(query)
		return rows

	def insert_bad_imgs(self, ide):
		query = ("INSERT INTO " + self.schema + ".bad_imgs "
								+ "(id) "
				  + " VALUES ("	+ str(ide) + ");")
		self.db_conn.execute_query(query)

	##########################################REAL COORDINATES ######################################
	def update_real_coordinates(self, ide, cbk_lat, cbk_lng, original_cbk_lat, original_cbk_lng):
		query = ("UPDATE "+self.schema+".coordinates SET "
				  + " cbk_lat = "+ cbk_lat +","
				  + " cbk_lng = "+ cbk_lng +","
				  + " original_cbk_lat = "+ original_cbk_lat +","
				  + " original_cbk_lng = "+ original_cbk_lng 
				  + " WHERE id = " + str(ide) + ";" )
		#print(query)
		self.db_conn.execute_query(query)


	##########################################TRAM ######################################	
	def get_tram_by_point(self, ide):
		query = ("SELECT tf.id_trams_openbcn,AVG(flow) FROM streetview_bcn.traffic_flow AS tf"
				+" INNER JOIN streetview_bcn.points_trams_openbcn AS  pt"
				+" ON tf.id_trams_openbcn = pt.id_trams_openbcn"
				+" WHERE id = " + str(ide) 
				+" GROUP BY tf.id_trams_openbcn;")
		rows = self.db_conn.execute_select(query)
		return rows

	def get_real_coordinates(self, limit, get_not_null=''):
		query = ("SELECT * FROM "+self.schema + ".coordinates WHERE cbk_lat IS "+get_not_null+" NULL ORDER BY random() LIMIT "+str(limit)+";")
		rows = self.db_conn.execute_select(query)

		#print(query)
		#for row in rows:
		#    print(row)
		return rows

	def close_database(self):
		self.db_conn.close_connection()


