#Filter of bad segmented images

import numpy as np
from shutil import copyfile
from utils import *
from db_controller import DB_Controller

WIDTH = 20000
n = 640
folder_path = '/Volumes/ExternalDiskSegate/espacio_persona/Barcelona/blended_images'
output_folder_path = '/Volumes/ExternalDiskSegate/espacio_persona/Barcelona/filtered'
folder_names = get_folder_names(folder_path) 

def classic_filter():
	db_control = DB_Controller()
	objects  = db_control.select_objects_image()

	i = 0
	for obj in objects:
		point_idx = obj[0]
		point_idx_str = str(point_idx)
		road_seg = obj[1]
		sidewalk_seg = obj[2]
		building_seg = obj[3]
		tree_seg = obj[9]
		car_seg = obj[14]

		objects_visibility_3d = db_control.select_objects_visibility_3d(point_idx)
		_,road_vis_3d,building_vis_3d,sidewalk_vis_3d,sky_vis_3d,road_sidewalk_3d = objects_visibility_3d[0]

		objects_3d = db_control.select_objects_3d(point_idx)
		_,road_3d,building_3d,sidewalk_3d,sky_3d = objects_3d[0]

		if (road_seg < 60000): # and building > 200000):
			if (road_vis_3d < 50000 and road_sidewalk_3d<20000):
				road_pct = road_vis_3d / road_3d

				folder_idx = int(point_idx/WIDTH)
				img_point_path = folder_names[folder_idx] + '/'+ point_idx_str + '.jpg'
				copyfile(img_point_path, output_folder_path+point_idx_str+".jpg")

		if (i%100 == 0):
			print(i)
		i += 1

	db_control.close_database()




#program image filter for several cases
'''
1. low road_pct and rs_sidewalk_pct superlow or NULL
2. road_pct<0.1 and rs_sidewalk_pct <0.1
3. road_pct 0 or null, rs_sidewalk_pct low, sidewalk 0 or low
4. sidewalk_vis 0 and rs_sidewalk_pct low
5. road_pct < 0.2 and rs_sidewalk_pct < 0.1
'''
db_control = DB_Controller()
objects  = db_control.get_values_objects_visibility()
i = 0
for obj in objects:
	point_idx, road, sidewalk, ratio, road_vis, sidewalk_vis, road_sidewalk, road_pct, rs_sidewalk_pct = obj
	point_idx_str = str(point_idx)
	folder_idx = int(point_idx/WIDTH)
	img_point_path = folder_names[folder_idx] + '/'+ point_idx_str + '.jpg'

	if rs_sidewalk_pct is None:
		rs_sidewalk_pct = 0

	if road_pct is None:
		road_pct = 0

	res = road_pct + rs_sidewalk_pct
	#1. low road_pct and rs_sidewalk_pct superlow or NULL  si
	if(road_pct < 0.2 and rs_sidewalk_pct < 0.1):
		copyfile(img_point_path, output_folder_path+'1/'+point_idx_str+".jpg")

	#2. road_pct + rs_sidewalk_pct < 0.3 si
	if(res < 0.4): 
		db_control.insert_bad_imgs(point_idx)
		copyfile(img_point_path, output_folder_path+'3/'+point_idx_str+".jpg")


	if (i%100 == 0):
		print(i)
	i += 1

db_control.close_database()
