from db_controller import DB_Controller
from utils import *
from glob import glob
import datetime
import unidecode
import os
import sys
import numpy as np
import time
import argparse
import csv

def insert_trams(input_file):
  file_content = read_file(input_file) 
  file_content = file_content[1:]
  
  #for line in csv.reader(file_content):
  for line in csv.reader(file_content, delimiter=';'):
  	ide = line[0]
  	des = unidecode.unidecode(line[1])
  	points = line[2].split(' ')
  	order_idx = 0
  	for p in points:
  		lat, lng, _ = p.split(',')
  		print(ide+';'+des+';'+str(order_idx)+';'+lat+';'+lng)
  		order_idx += 1

#Estimacion del flujo de trafico por los tramos principales de Barcelona
def insert_traffic_flow(input_folder):
  info_files = glob(input_folder+'/*.csv') 
  trams = 534 + 1
  months = 12 + 1
  hours = 24
  day_types = 2
  flow_data = np.zeros((trams,months,hours,day_types,1)) # month, hour, day type: week/weekend, total
  flow_data_freq = np.zeros((trams,months,hours,day_types,1)) # month, hour, day type: week/weekend, frequency
  i = 0
  for f in info_files:
    file_content = read_file(f) 
    file_content = file_content[1:]
    print(len(file_content))

    
    for line in csv.reader(file_content, delimiter=','):
      tram_id = int(line[0])
      timestamp = line[1]
      traffic = int(line[2])

      year = int(timestamp[:4])
      month = int(timestamp[4:6])
      day = int(timestamp[6:8])
      hour = int(timestamp[8:10])

      #know if the day from date is a week or weekend
      date = datetime.date(year, month, day)
      weekday = date.weekday()
      day_type = 0 if weekday >= 5 else 1

      #print(tram_id,timestamp, traffic)
      #print(year,month,day,hour)
      if(tram_id == 1 & month == 11 & hour == 8 & day_type == 1):
        i  += 1
      try:
        flow_data[tram_id][month][hour][day_type] += traffic
        flow_data_freq[tram_id][month][hour][day_type] += 1
      except:
        continue 

  
 
  db_control = DB_Controller()
  for t in np.arange(1,trams): #id_trams
    for m in np.arange(1,months): #months
      for h in np.arange(hours): #hours
        for d in np.arange(day_types):

          value = flow_data[t][m][h][d][0]
          freq = flow_data_freq[t][m][h][d][0]

          if(freq > 0):
            year = 2017 if m >= 10 else 2018 #solo tenemos datos desde octubre 2017
            

            flow = round(value/freq)
            query = ("INSERT INTO streetview_bcn.traffic_flow (id_trams_openbcn,year,month,weekday,hour,flow)" 
                      + " VALUES ("+ str(t) + "," + str(year) + "," + str(m) + ",'" + str(d) + "'," + str(h) + "," + str(flow) + ");")
            try:
              db_control.exe_query(query)
            except:
              print("exception in " + query)
              continue
            
  db_control.close_database()            
  return flow_data, flow_data_freq
      
    



if __name__ == "__main__":
  # measure time
  start_time = time.time()
  path = '/Users/cristina/Documents/Code/dgt_project/google_street_view/BCN_Open_Databases/'
  parser = argparse.ArgumentParser()  
  parser.add_argument('--input_file', type=str, default=path+'Tramos/transit_relacio_trams 2.csv')
  parser.add_argument('--input_folder', type=str, default=path+'Transito_tramo_historico')
  #parser.add_argument('--output_folder', type=str, default=default_folder_name())
  args = parser.parse_args()

  input_file = args.input_file
  input_folder = args.input_folder

  #insert_trams(input_file)
  flow_data, flow_data_freq = insert_traffic_flow(input_folder)
  #print("--- %s seconds ---" % (time.time() - start_time))