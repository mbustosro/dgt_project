#Number of accidents by radio

import numpy as np
import matplotlib.pyplot as plt
from db_controller import DB_Controller

db_control = DB_Controller()
radio = 200
data = db_control.number_accidents_by_radio(radio)
data = np.asarray(data)
x = data[:,0]
y = data[:,1]

n = 177645
y_0 = max(n - np.sum(y), 0)

x=np.insert(x,0,0)
y=np.insert(y,0,y_0)

print(x)
print(y)

plt.figure(1, figsize=(8, 6))
plt.suptitle('Radio '+str(radio), fontsize="x-large")
# linear
plt.subplot(121)
plt.bar(x, y)
plt.yscale('linear')
plt.title('Linear')
plt.xlabel('Number of accidents')
plt.ylabel('Frequency of points')
plt.grid(True)

plt.subplot(122)
plt.bar(x, y, log=True)
#plt.yscale('log')
plt.title('Log')
plt.xlabel('Number of accidents')
plt.grid(True)

#plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.25,
#                    wspace=0.35)
plt.savefig('radio_'+str(radio)+'.png')
plt.show()