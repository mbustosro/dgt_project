labels = np.array(('0','1'))
colors = ['turquoise', 'darkorange']
lw = 2
pca = PCA(n_components=2)
X_r = pca.fit(X).transform(X)
plt.figure()
for color, i, label in zip(colors, [0,1], labels):
	plt.scatter(X_r[y == i, 0], X_r[y == i, 1], color=color, alpha=.3, lw=lw, label=label)

plt.legend(loc='best', shadow=False, scatterpoints=1)
plt.title('PCA')
plt.show()



labels = np.array(('0','1'))
colors = ['turquoise', 'darkorange']
lw = 2
pca = PCA(n_components=3)
X_r = pca.fit(X).transform(X)
plt.figure()
for color, i, label in zip(colors, [0,1], labels):
	plt.scatter(X_r[y == i, 0], X_r[y == i, 1], X_r[y == i, 2], color=color, alpha=.3, lw=lw, label=label)

plt.legend(loc='best', shadow=False, scatterpoints=1)
plt.title('PCA')
plt.show()

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure(1, figsize=(4, 3))
plt.clf()
ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)
plt.cla()
for name, label in [('No', 0), ('Si', 1)]:
    ax.text3D(X_r[y == label, 0].mean(),
              X_r[y == label, 1].mean() + 1.5,
              X_r[y == label, 2].mean(), name,
              horizontalalignment='center',
              bbox=dict(alpha=.5, edgecolor='w', facecolor='w'))
# Reorder the labels to have colors matching the cluster results
y = np.choose(y, [1, 2, 0]).astype(np.float)
ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=y, cmap=plt.cm.spectral,
           edgecolor='k')
ax.w_xaxis.set_ticklabels([])
ax.w_yaxis.set_ticklabels([])
ax.w_zaxis.set_ticklabels([])

plt.show()

tsne = manifold.TSNE(n_components=3, init='pca', random_state=0)
X_tsne = tsne.fit_transform(X)
fig = plt.figure(1, figsize=(4, 3))
plt.clf()
ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)
plt.cla()
for name, label in [('No', 0), ('Si', 1)]:
    ax.text3D(X_tsne[y == label, 0].mean(),
              X_tsne[y == label, 1].mean() + 1.5,
              X_tsne[y == label, 2].mean(), name,
              horizontalalignment='center',
              bbox=dict(alpha=.5, edgecolor='w', facecolor='w'))

y = np.choose(y, [1, 2, 0]).astype(np.float)
ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=y, cmap=plt.cm.spectral,edgecolor='k')
ax.w_xaxis.set_ticklabels([])
ax.w_yaxis.set_ticklabels([])
ax.w_zaxis.set_ticklabels([])

plt.show()

#Reducing features according their score and variance
selector = RFE(svc, 3, step=1)
selector = selector.fit(X,y)
np.vstack((feature_names,selector.ranking_)).T
np.vstack((feature_names,selector.support_)).T
X_t = selector.transform(X)
selector.score(X, y)