from glob import glob
import argparse
import os 


if __name__ == "__main__":
  # measure time
  parser = argparse.ArgumentParser()  
  parser.add_argument('--input_folder', type=str, default='')
  args = parser.parse_args()

  input_folder = args.input_folder
  #/Volumes/ExternalDiskSegate/espacio_persona/Barcelona/gsv_images
  #/Volumes/ExternalDiskSegate/espacio_persona/Madrid/gsv_images
  total_points = 1800000 #Madrid
  #total_points = 600000
  width = 20000
  ext = '/*.jpg'
  folder_names = []
  for i in range(total_points):
    if(i%width==0):
      folder_name = str(i)+'-'+str(i+width)
      folder_path = os.path.join(input_folder,folder_name)

      folder_names.append(folder_path)
      print(folder_path)
      if not os.path.isdir(folder_path):
        os.makedirs(folder_path)


  images = glob(input_folder+ext) 
  #initial_name =  
  n=len(images)
  for i, img_path in enumerate(images):
    img_name = os.path.basename(img_path)
    img_idx = int(img_name[:-4])
    
    folder_idx = int(img_idx/width)
      
      
    #print(i)
    new_name = folder_names[folder_idx] + '/'+ img_name
    os.rename(img_path, new_name)
    print(folder_names[folder_idx],'  ',img_idx)
    