#Machine learning classification
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn import manifold, preprocessing
from sklearn.model_selection import train_test_split
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis


def plot_coef(clf, feature_names, top_features=10):
	coef = clf.coef_.ravel()
	top_positive_coefficients = np.argsort(coef)[-top_features:]
	top_negative_coefficients = np.argsort(coef)[:top_features]
	top_coefficients = np.hstack([top_negative_coefficients, top_positive_coefficients])
	# create plot
	plt.figure(figsize=(10, 5))
	colors = ['red' if c < 0 else 'blue' for c in coef[top_coefficients]]
	plt.bar(np.arange(2 * top_features), coef[top_coefficients], color=colors)
	feature_names = np.array(feature_names)
	plt.xticks(np.arange(1 + 2 * top_features), feature_names[top_coefficients], rotation=45, ha='center')
	plt.show()


def classification():
	feature_names =   ['road', 'sidewalk','building','wall','fence' ,'pole','traffic light','traffic sign',
						'vegetation' ,'terrain' ,'sky','person','rider','car','truck','bus','train','motorcycle','bicycle']
	#features_positives_tram=np.load('features_positives_tram.npy')
	#features_negatives_tram=np.load('features_negatives_tram.npy')

	
	features_positives=np.load('features_positives_not_tram.npy')
	features_negatives=np.load('features_negatives_not_tram.npy')
	'''
	#add feature of traffic
	features_positives_tram = np.concatenate((features_positives_tram,np.ones((features_positives_tram.shape[0],1))),axis=1)
	features_negatives_tram = np.concatenate((features_negatives_tram,np.ones((features_negatives_tram.shape[0],1))),axis=1)
	features_positives_not_tram = np.concatenate((features_positives_not_tram,np.zeros((features_positives_not_tram.shape[0],1))),axis=1)
	features_negatives_not_tram = np.concatenate((features_negatives_not_tram,np.zeros((features_negatives_not_tram.shape[0],1))),axis=1)

	features_positives = np.concatenate((features_positives_tram,features_positives_not_tram), axis=0)
	features_negatives = np.concatenate((features_negatives_tram,features_negatives_not_tram), axis=0)
	'''
	np.random.shuffle(features_positives)
	np.random.shuffle(features_negatives)

	X = np.concatenate((features_negatives, features_positives), axis=0)
	y1 = np.full((features_negatives.shape[0]), 0)
	y2 = np.full((features_positives.shape[0]), 1)
	y = np.concatenate((y1, y2))

	#remove moving objects
	#X = np.delete(X, [11,12,13,14,15,16,17,18], axis=1)
	#feature_names = np.delete(feature_names, [11,12,13,14,15,16,17,18])

	#normalize data
	scaler = preprocessing.StandardScaler()
	scaler.fit(X)
	X = scaler.transform(X)

	#Split the data
	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)

	#LDA for Classification
	lda = LinearDiscriminantAnalysis(solver="svd", store_covariance=True)
	y_pred_lda = lda.fit(X_train, y_train).predict(X)
	lda_score = lda.score(X_test,y_test)
	print('lda_score ',lda_score)

	'''
	#QDA for Classification
	qda = QuadraticDiscriminantAnalysis(store_covariances=True)
	y_pred_qda = qda.fit(X_train, y_train).predict(X)
	qda_score = qda.score(X_test,y_test)
	print('qda_score ',qda_score)
	'''

	#SVM
	svc = SVC(C=1.0, kernel='linear')
	svc.fit(X_train, y_train)
	y_pred_svc = svc.predict(X)
	svc_score = svc.score(X_test,y_test)
	print('svc_score ', svc_score)

	plot_coef(svc,feature_names,top_features=10)
