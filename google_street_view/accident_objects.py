from db_controller import DB_Controller
from cityscapes_labels import trainId2label
from scipy import misc, ndimage
from utils import *
import os
import sys
import numpy as np
import time
import argparse
from shutil import copy2
import matplotlib.pyplot as plt
from scipy import stats
from skimage import measure

WIDTH = 20000
class_id_to_rgb_map = trainId2label
categories = 19
total_pixels = 640*640

def get_folder_names(input_folder, total_points=600000):
  folder_names = []
  for i in np.arange(0,total_points,WIDTH):
    if(i%WIDTH==0):
      folder_name = str(i)+'-'+str(i+WIDTH)
      folder_path = os.path.join(input_folder,folder_name)
      folder_names.append(folder_path)
  return folder_names

def get_points_by_accidents():
  db_control = DB_Controller()
  accident_points =  db_control.select_points_with_accidents()
  db_control.close_database()
  return accident_points

def get_points_without_accidents():
  db_control = DB_Controller()
  points =  db_control.select_points_without_accidents()
  db_control.close_database()
  return points


def calculate_objects_image(points, input_folder):
  db_control = DB_Controller()

  total_points = 1800000 #Madrid
  folder_names = get_folder_names(input_folder, total_points)
  #print(class_id_to_rgb_map)
  i = 0
  for p in points:

    point_idx = p[0]
    point_idx_str = str(point_idx)
    folder_idx = int(point_idx/WIDTH)
    img_point_path = folder_names[folder_idx] + '/'+ str(point_idx) + '.png'
    #img_point_path = input_folder + point_idx_str + '.png'
    #print (img_point_path)
    #copy2(img_point_path, '/Users/cristina/Documents/Code/dgt_project/google_street_view/img_accidents/')
    objects_img = db_control.select_objects_image_by_id(point_idx)
    if len(objects_img) > 0:
      continue

    try:
      img = misc.imread(img_point_path)
    except:
      print('no existe la imagen '+ img_point_path)
      continue

    #create a dictionary for counting the objects in the image
    objects = dict(zip(np.arange(categories),np.zeros(categories)))

    freq = np.bincount(img.flatten(),minlength=18)
    ii = np.nonzero(freq)[0]
    objects_freq = np.vstack((ii,freq[ii])).T

    for o in objects_freq:
      o_idx = o[0]
      o_value = o[1]
      objects[o_idx] = o_value
    
    #index are from cityscapes dataset
    road          = objects[0]
    sidewalk      = objects[1]
    building      = objects[2]
    wall          = objects[3]
    fence         = objects[4]
    pole          = objects[5]
    traffic_light = objects[6]
    traffic_sign  = objects[7]
    vegetation    = objects[8]
    terrain       = objects[9]
    sky           = objects[10]
    person        = objects[11]
    rider         = objects[12]
    car           = objects[13]
    truck         = objects[14]
    bus           = objects[15]
    train         = objects[16]
    motorcycle    = objects[17]
    bicycle       = objects[18]

    db_control.insert_objects_image(point_idx, road, sidewalk, building, wall, fence, pole, traffic_light, traffic_sign, 
                vegetation, terrain, sky, person, rider, car, truck, bus, train, motorcycle, bicycle)
    
    if(i%100 == 0):
      print('i:', i)
    i = i+1
  #print(objects_img)
  db_control.close_database()


def calculate_entropy_by_image(points):
  db_control = DB_Controller()

  folder_names = get_folder_names(input_folder)
  #print(class_id_to_rgb_map)
  n = len(points)
  entropy = [0] * n
  i = 0
  for p in points:

    point_idx = p[0]
    point_idx_str = str(point_idx)
    folder_idx = int(point_idx/WIDTH)
    img_point_path = folder_names[folder_idx] + '/'+ str(point_idx) + '.png'
    #img_point_path = input_folder + point_idx_str + '.png'
    #print (img_point_path)
    #copy2(img_point_path, '/Users/cristina/Documents/Code/dgt_project/google_street_view/img_accidents/')
    try:
      img = misc.imread(img_point_path)
    except:
      print('no existe la imagen '+ img_point_path)
      i = i+1
      continue


    entropy[i]=measure.shannon_entropy(img)
    i = i+1
    if(i%100 == 0):
      print('entropy index :', i)
  
  return entropy



def get_objects_percentages(points):
  db_control = DB_Controller()
  #arrays for plots
  n = len(points)
  accidents = [0] * n
  sidewalk = [0] * n
  road = [0] * n
  vehicles = [0] * n
  i = 0
  for p in points:
    #print(point)
    ide = p[0]
    objects_img = db_control.select_objects_image_by_id(ide)
    #print(objects_img)
    if len(objects_img) > 0:
      objects_img = objects_img[0]
      all_vehicles = np.sum(objects_img[13:])

      #accidents.append(point[1])
      road[i] = objects_img[1] / total_pixels * 100
      sidewalk[i] = objects_img[2] / total_pixels * 100
      vehicles[i] = all_vehicles / total_pixels * 100
    i = i + 1

  db_control.close_database()
  return road, sidewalk, vehicles


def calculate_all_objects(input_folder):
  db_control = DB_Controller()
  points = db_control.select_unique_points_with_image()
  print('total_unique_points ',len(points))
  calculate_objects_image(points, input_folder)
  db_control.close_database()



if __name__ == "__main__":
  # measure time
  start_time = time.time()
  
  parser = argparse.ArgumentParser()  
  parser.add_argument('--input_folder', type=str, default='/Volumes/ExternalDiskSegate/espacio_persona/Madrid/seg_read_images/')
  #parser.add_argument('--output_folder', type=str, default=default_folder_name())
  args = parser.parse_args()

  input_folder = args.input_folder
  calculate_all_objects(input_folder)


  #pts_accidents = get_points_by_accidents()
  #pts_no_accidents = get_points_without_accidents()
  #calculate_objects_image(accidents_points, input_folder)
  

  
  #ENTROPY
  '''
  pts_accidents_entropy = calculate_entropy_by_image(pts_accidents)
  pts_no_accidents_entropy = calculate_entropy_by_image(pts_no_accidents)

  np.save('pts_accidents_entropy.npy',pts_accidents_entropy)
  np.save('pts_no_accidents_entropy.npy',pts_no_accidents_entropy)

  
  #OBJECTS
  road_accidents, sidewalk_accidents, vehicles_accidents = get_objects_percentages(pts_accidents)
  np.save('road_accidents.npy',road_accidents)
  np.save('sidewalk_accidents.npy',sidewalk_accidents)
  np.save('vehicles_accidents.npy',vehicles_accidents)
  road, sidewalk, vehicles = get_objects_percentages(pts_no_accidents)
  np.save('road.npy',road)
  np.save('sidewalk.npy',sidewalk)
  np.save('vehicles.npy',vehicles)
  

  pts_accidents_entropy = np.load('pts_accidents_entropy.npy')
  pts_no_accidents_entropy = np.load('pts_no_accidents_entropy.npy')
  road_accidents = np.load('road_accidents.npy')
  sidewalk_accidents = np.load('sidewalk_accidents.npy')
  vehicles_accidents = np.load('vehicles_accidents.npy')
  road_no_accidents = np.load('road.npy')
  sidewalk_no_accidents = np.load('sidewalk.npy')
  vehicles_no_accidents = np.load('vehicles.npy')

  #concatenate
  accidents_0 = np.zeros(len(pts_no_accidents_entropy))
  accidents = np.asarray([p[1] for p in pts_accidents])
  total_accidents = np.concatenate((accidents_0,accidents))

  entropy = np.concatenate((pts_no_accidents_entropy,pts_accidents_entropy))
  road = np.concatenate((road_no_accidents,road_accidents))
  sidewalk = np.concatenate((sidewalk_no_accidents,sidewalk_accidents))
  vehicles = np.concatenate((vehicles_no_accidents,vehicles_accidents))
  '''







































  '''
  n_bins = 100
  bin_means, bin_edges, binnumber=stats.binned_statistic(accidents,road, statistic='mean', bins=n_bins)
  plt.bar(bin_edges[:-1],bin_means)
  plt.title('mean road')
  plt.ylabel('Porcentaje de Road')
  plt.xlabel('Numero Accidentes')
  plt.savefig('mean_road.png')
  plt.show()

  #THIS is for create the data for the box plot
  data_accidents = []
  accidents_array = np.asarray(accidents)
  sidewalk_array = np.asarray(sidewalk)
  for i in range(1,n_bins+1):
    bin_places = np.where(accidents_array == i)
    data_accidents.append(sidewalk_array[bin_places].tolist())




  #FIGURE
  bin_means = np.nan_to_num(bin_means)
  x_labels_array = np.arange(1,n_bins+1)
  fig = plt.figure(1, figsize=(25, 20))
  plt.boxplot(data_accidents)
  plt.xticks(x_labels_array, [str(i) for i in x_labels_array], rotation=45)
  plt.title('Porcentaje de Acera por Accidentes de peatones')
  plt.xlabel('Numero de accidentes de Peatones')
  plt.ylabel('Porcentaje de Acera')
  fig.savefig('cajas_acera.png', bbox_inches='tight')
  plt.show()


  plt.hist2d(accidents,road, bins=(14,50))
  plt.title('Porcentaje de Asfalto en la Imagen por Accidentes de peatones')
  plt.xlabel('Numero de Accidentes de Peatones')
  plt.ylabel('Porcentaje de Asfalto en la Imagen')
  plt.colorbar()
  plt.savefig('hist2d_asfalto.png')
  plt.show()


  #print("--- %s seconds ---" % (time.time() - start_time))
  '''