from db_controller import DB_Controller
from utils import *
from math import sqrt
import os
import sys
import numpy as np
import time
import argparse
import csv
import utm

def insert_accident(input_file):
  file_content = read_file(input_file) 
  file_content = file_content[4:]
  
  #for line in csv.reader(file_content):
  for line in csv.reader(file_content, delimiter=';'):
    db_control = DB_Controller()
    exp_id = line[0]
    year = line[11]

    #for 2014
    utm_y = line[23]
    utm_x = line[24]

    '''
    #for 2017 & 2016
    utm_x = line[23]
    utm_y = line[24]
    exp_id = exp_id[:-4]
    lng = line[25]
    lat = line[26]
    '''

    utm_x = utm_x.replace(',','.')
    utm_y = utm_y.replace(',','.')
    

    if (float(utm_x) == -1 or float(utm_y) == -1):
      continue
    lat, lng = utm.to_latlon(float(utm_x), float(utm_y), 31, 'N')
    
    #print(exp_id, year, utm_x, utm_y, lat, lng)
    db_control.insert_accidents_locations(exp_id, year, utm_x, utm_y, str(lat), str(lng))
    db_control.close_database()


def insert_accident_people(input_file):
  file_content = read_file(input_file) 
  file_content = file_content[1:]
  for line in csv.reader(file_content, delimiter=','):
    db_control = DB_Controller()
    db_control.set_schema('streetview_bcn')
    exp_id = line[0]

    ###for 2017 & 2016
    #person_type = line[21]
    #exp_id = exp_id[:-4]
    #genero = line[18]

    ###for 2015 & 2014
    #person_type = line[19]
    #age = line[20]
    #genero = line[18]


    ###for 2013,2012,2011,2010
    exp_id = exp_id[:-4]
    person_type = line[20]
    age = line[21]
    genero = line[19]

    if age=='Desconegut':
      age = 'NULL'
    genre = 'F' if genero == 'Dona' else 'M' 

    #print(exp_id,'-',person_type,'-',genre,'-',age)
    db_control.insert_accidents_people(exp_id, person_type, age, genre)
    db_control.close_database()
 

def find_closest_point():
  db_control = DB_Controller()
  accidents  = db_control.select_accidents_locations()
  points =  db_control.select_unique_points_with_image()
  db_control.close_database()
  #print(len(accidents))
  #print(len(points))
  for a in accidents:
    exp_id = a[0]
    a_lat = float(a[1]) #latitud
    a_lng = float(a[2]) #longitud
    closest = 10000000
    lat_closest = 0
    lng_closest = 0
    ide_closest = ''

    for p in points:
      p_ide = p[0]
      p_lat = float(p[2])
      p_lng = float(p[3])
      dist = sqrt( (a_lat - p_lat)**2 + (a_lng - p_lng)**2 )

      #print('closest',closest, ' dist ', dist)
      if(dist < closest):
        closest = dist
        ide_closest = p_ide
        lat_closest = p_lat
        lng_closest = p_lng

    db_control = DB_Controller()    
    db_control.insert_closest_points(exp_id, str(ide_closest), str(closest))
    db_control.close_database()
    #print(a_lat, '-', lat_closest, ' *** ', a_lng, '-', lng_closest, ' dist: ', closest)
  


if __name__ == "__main__":
  # measure time
  start_time = time.time()
  
  parser = argparse.ArgumentParser()  
  parser.add_argument('--input_file', type=str, default='input.txt')
  #parser.add_argument('--output_folder', type=str, default=default_folder_name())
  args = parser.parse_args()

  input_file = args.input_file
  #download_folder = args.output_folder

  insert_accident_people(input_file)
  print("--- %s seconds ---" % (time.time() - start_time))