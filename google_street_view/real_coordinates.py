from GSV_Image import GSV_Image
from db_controller import DB_Controller
from utils import *
from xml.etree import ElementTree
from shutil import copyfile
import numpy as np
import requests
import argparse
import time

URL = 'http://cbk0.google.com/cbk?output=xml&ll='
#example 'http://cbk0.google.com/cbk?output=xml&ll=41.41128,2.185785'

def get_seconds_to_sleep():
  total_seconds_x_day = 86400
  #total_seconds_x_day = 70000
  total_samples_x_day = 70000
  size_interval = total_seconds_x_day / total_samples_x_day
  samples = np.random.poisson(size_interval, 1)
  return samples[0]

def update_coordinates():
  db_control = DB_Controller()
  db_control.set_schema('streetview_bcn')
  points  = db_control.select_unique_points_with_image()
  points  = db_control.get_real_coordinates(100000)
  db_control.close_database()
  i = 0 
  for p in points:
    if(i%200 == 0):
      print('processed ',i,' points')
    ide = p[0]
    lat = str(p[1])
    lng = str(p[2])
    url_params = URL + lat + ',' + lng
    #print(url_params)

    #getting response
    #time.sleep(get_seconds_to_sleep())
    response = requests.get(url_params, stream=True)
    panorama = ElementTree.fromstring(response.content)

    cbk_lat = cbk_lat = cbk_lng = original_cbk_lat = original_cbk_lng = ''
    for data in panorama.findall('data_properties'):
      cbk_lat = data.get('lat')
      cbk_lng = data.get('lng')
      original_cbk_lat = data.get('original_lat')
      original_cbk_lng = data.get('original_lng')
    i = i+1
    try:
      if not cbk_lat:
        cbk_lat = cbk_lng = original_cbk_lat = original_cbk_lng = '0'
      db_control = DB_Controller()
      db_control.set_schema('streetview_bcn')
      db_control.update_real_coordinates(ide, cbk_lat, cbk_lng, original_cbk_lat, original_cbk_lng)
      db_control.close_database()
    except:
      continue

  


def get_random_coordinates(input_folder):
  n_samples = 1000
  WIDTH = 20000
  folder_names = get_folder_names(input_folder)
  filename = 'all_coordinates.txt'
  output = open(filename,'a+')

  db_control = DB_Controller()
  db_control.set_schema('streetview_bcn')
  points  = db_control.get_real_coordinates(n_samples, get_not_null='NOT')
  for point in points:
    point_idx = point[0]
    point_idx_str = str(point_idx)
    folder_idx = int(point_idx/WIDTH)
    img_point_path = folder_names[folder_idx] + '/'+ str(point_idx) + '.jpg'
    
    for p in point:
      output.write(str(p)+" ")
    
    output.write(point_idx_str+".jpg \n")
    copyfile(img_point_path,"coordinates/"+point_idx_str+".jpg")
    print(img_point_path)

  output.close()
  db_control.close_database()


###############################################################################################

if __name__ == "__main__":
  # measure time
  start_time = time.time()
  update_coordinates()
  parser = argparse.ArgumentParser()  
  parser.add_argument('--input_folder', type=str, default='/Volumes/ExternalDiskSegate/espacio_persona/Barcelona/gsv_images/')
  #parser.add_argument('--output_folder', type=str, default=default_folder_name())
  args = parser.parse_args()

  input_folder = args.input_folder

  #get_random_coordinates(input_folder)
  
  print("--- %s seconds ---" % (time.time() - start_time))