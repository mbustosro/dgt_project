from GSV_Image import GSV_Image
from utils import *
import os
import sys
import time
import numpy as np
import time
import argparse

def download_images(args):
  '''
  Read file for getting the each image parameters 
  example of file.txt

  id lat lng heading pitch
  1 41.3958516 2.1810987 315.32 0
  2 41.3953451 2.170443 314.33 0
  3 41.3969763 2.1712185 224.92 0

  '''

  #input parameters
  filename = args.input_file
  output_folder = args.output_folder
  begin_index = args.begin_index
  number_queries = args.number_queries
  output_filename = args.output_file
  user_key = args.user_key


  if os.path.exists(output_folder) == False:
          os.mkdir(output_folder)
  print('download folder: ',output_folder)

  #Reading file
  file_content = read_file(filename) 
  number_lines = len(file_content)
  print('Number of input points ', number_lines - 1)

  n = min(begin_index + number_queries, number_lines) 
  #preinitialized the list of queries
  #each element of this list will be a query for google street api, each query must contain at least image size, location, key
  #if parameters such as heading, pitch are not provided, google street api will take the default value
  queries = [] 
  GSV_imgs = [] 
  gsv_img = None    # Var will be modify in the loop

  ##sequential
  for i in range(begin_index, n): 

    items = file_content[i].split() #split by blank spaces ' '
    ide, lat, lng, heading, pitch = items
    gsv_img = GSV_Image(ide, lat, lng, heading, pitch)
    GSV_imgs.append(gsv_img)
    queries.append(gsv_img.get_gsv_query(user_key))

  for q in queries:
    print(q)

  #parallel
  #Sending queries to google google_streetview
  responses = make_google_street_view_queries(queries) #todo queries free qouta
  #print(responses)

  print('Already made queries of all points')  
  # Look for repeated images
  # for doing this, we use a python structure set(), internally it has the structure of uniqueness
  # The criteria of repeated elements is based on a 3 order tuple composed of (lat, lng, heading)

  #seen = load_seen_images(output_filename) 
  user_key = 'AIzaSyDB09RFL6NUVAOY9frsCxrPRxVNE2JdpoA'
  seen = set()
  unique_queries = []
  metadata_unique_queries = []

  #output file
  output = open(os.path.join(output_folder,output_filename),'w+') 
  output.write(output_file_heading())
  number_queries = len(queries)
  img_name_idx = 1
  copyright = ''

  for i in range(number_queries):

    GSV_imgs[i].set_status(responses[i]['status'])
    if(i%1000 == 0):
      print('processed '+str(i)+' images')

    if(GSV_imgs[i].status == 'OK'):

      copyright = responses[i]['copyright']
      copyright = copyright[2:] #remove 2 first stranger caracters

      if(copyright == 'Google, Inc.'):
        GSV_imgs[i].set_exact_location(responses[i]['location'])
        exact_location = GSV_imgs[i].get_exact_location_tuple()
        ide, exact_lat, exact_lng, exact_hdg = exact_location
        ########(INSTEAD OF (if exact_location not in seen):)#######
        matches = ([x for x in seen if (x[1],x[2]) == (exact_lat, exact_lng) and abs(exact_hdg - x[3]) < 10 ])

        '''
        print('seen ', seen)
        print('index', i)
        print('current element: ', exact_location)
        if len(matches) == 0:
          print('no matches')
        else:   
          print('matches', matches)
        '''      
        #print(seen)
        #if exact_location not in seen:
        if not matches:
          seen.add(exact_location)
          unique_queries.append(GSV_imgs[i].get_new_gsv_query(user_key))
          metadata_unique_queries.append(responses[i])
          output.write(GSV_imgs[i].get_output_file_info(img_name_idx=str(img_name_idx)))
          img_name_idx += 1
        else:
          GSV_imgs[i].set_seen_id(matches[0][0])
          output.write(GSV_imgs[i].get_output_file_info(seen=True))
      else:
        GSV_imgs[i].set_status('INVALID_COPYRIGHT')
        output.write(GSV_imgs[i].get_output_file_info())

    else:
        output.write(GSV_imgs[i].get_output_file_info())  


  #print(unique_queries)
  #print(metadata_unique_queries)

  output.close()
  uq = len(unique_queries)
  print('number of repeated images ', number_queries - uq)
  print('number of new queries ',uq)
  print('image_index ', img_name_idx)

  download_gsv_images(unique_queries, metadata_unique_queries, output_folder)


if __name__ == "__main__":
  # measure time
  start_time = time.time()
  parser = argparse.ArgumentParser()
  output_folder = default_folder_name()
  #key for mcb9216@gmail.com user https://developers.google.com/maps/documentation/streetview/get-api-key?hl=es-419
  #user_key  = 'AIzaSyATzGBZcOc5ZL6eke6fE0nC7C5Wc4HIg8A'
  #'AIzaSyAAx6TZh0Q8Kv12oU6CmwxT7cf5o5-Lg8o'
  user_key = 'AIzaSyBgzOkNeKfkY_B71zPOoRpwWsIVSYZhqoE'

  parser.add_argument('--input_file', type=str, default='input.txt')
  parser.add_argument('--output_file', type=str, default='output.txt')
  parser.add_argument('--output_folder', type=str, default=output_folder) 
  parser.add_argument('--begin_index', type=int, default=1) 
  parser.add_argument('--number_queries', type=int, default=25000) 
  parser.add_argument('--user_key', type=str, default=user_key) 
  args = parser.parse_args()
  download_images(args)
  print("--- %s seconds ---" % (time.time() - start_time))