from GSV_Image import GSV_Image
from db_controller import DB_Controller
from utils import *
from datetime import datetime, timedelta
import multiprocessing as mp
import os
import sys
import numpy as np
import time
import argparse
import json
import schedule

###################### Insert points into database ######################
def read_file_insert_points(filename):

  #Open database controller
  db_control = DB_Controller()
  file_content = read_file(filename) 
  number_points = len(file_content)
  print('Number of input points ', number_points - 1)

  ide = lat = lng = heading = pitch = ''

  for i in range(1, number_points): 

    items = file_content[i].split() #split by blank spaces ' '
    ide, lat, lng, heading, pitch = items
    db_control.insert_search_points(ide, lat, lng, heading, pitch) #insert point in the database
    db_control.insert_returned_points(ide)


  print("points inserted ")
  #db_control.select_search_points()
  db_control.close_database()

###################### Make initial request to gsv api ######################

def get_not_processed_points(requests_limit):
  db_control = DB_Controller()
  points = db_control.select_not_processed_points(requests_limit)
  db_control.close_database()
  return points

def block_not_processed_points(points):
  db_control = DB_Controller()
  ide = ''
  for p in points:
    ide = p[0]
    db_control.update_search_points_in_process(ide) #assign points status to: 'IN PROCESS'
  db_control.close_database()

#process to be run paralelly
def make_initial_google_requests(points, user_key):
  db_control = DB_Controller()

  #print('processed started at ', time.asctime())
  ide = lat = lng = heading = pitch = gsv_query = response = ''
  for p in points:
    ide, lat, lng, heading, pitch = p[:-2] #get all the vales unless the last 2
    gsv_query = get_gsv_query(lat, lng, heading, pitch, user_key)
    response  = make_google_street_view_queries(gsv_query)
    #response  = str(response[0])
    db_control.update_search_points_response(ide, response[0]) 
    db_control.update_returned_point_status(ide, 'READY')

  db_control.close_database()


##################LOOK FOR REPEATED ELEMENTS ################

def look_for_repeated_responses():
  db_control = DB_Controller()
  points = db_control.select_points_ready_for_process()
  #initialize vars
  ide = lat = lng = heading = pitch = processed_status = response = ''
  exact_lat = exact_lng = copyright = response_status = ''

  for p in points:
    point = db_control.select_search_point(p[0])
    ide, lat, lng, heading, pitch, processed_status, response = point
    response = json.loads(response)
    response_status = response['status']

    if(response_status == 'OK'):
      copyright = response['copyright']
      copyright = copyright[2:] #remove 2 first stranger caracters

      if(copyright == 'Google, Inc.'):
        exact_lat = response['location']['lat']
        exact_lng = response['location']['lng']
        matches = db_control.look_for_location_matches(exact_lat,exact_lng,heading)

        if len(matches) == 0:
          response_status = 'UNIQUE'
          db_control.update_unique_point(ide, response_status, exact_lat, exact_lng, heading, pitch)
        else:
          repeated_id = matches[0][0]
          response_status = 'REPEATED'
          db_control.update_repeated_point(ide, response_status, repeated_id)
          #add repeated

      else:
          response_status = 'INVALID_COPYRIGHT'
          db_control.update_returned_point_status(ide,response_status)
    else:
      db_control.update_returned_point_status(ide,response_status)
    #print (response_status)
    #

  db_control.close_database()

##################################################### DOWNLOAD IMAGES #########################################################
def get_unique_points_without_image(requests_limit):
  db_control = DB_Controller()
  points = db_control.select_unique_points_without_image(requests_limit)
  db_control.close_database()
  return points

def block_not_download_image_points(points):
  db_control = DB_Controller()
  ide = ''
  for p in points:
    ide = p[0]
    db_control.update_image_info_in_process(ide) #assign points status to: 'IN PROCESS'

  db_control.close_database()


def download_images(points, download_folder, user_key):
  db_control = DB_Controller()
  #pedir parametros a la database -- status should be 'unique'
  #colocar aqui el cambio a status 'c' para el batch de 25mil
  ide = lat = lng = heading = pitch = img_name = ''
  for p in points:
    ide, lat, lng, heading, pitch = p 
    img_name = str(ide) + '.jpg'
    gsv_query = get_gsv_query(lat, lng, heading, pitch, user_key)
    download_single_gsv_image(gsv_query, download_folder, img_name)
    #if(os.path.exists(download_folder+'/'+img_name):
    db_control.update_image_info(ide,img_name)
    #else:
    #  db_control.update_image_info_to_neg(ide)

  db_control.close_database()


def download_images_with_invalid_copyright(download_folder):
  user_key = 'AIzaSyBELFMT-DH0AIcRv-_-8hClNd2mnYG-rkg' #for some strange reason, google api can not download an image with a private key
  db_control = DB_Controller()
  points = db_control.select_invalid_copyright_points()
  ide = lat = lng = heading = pitch = img_name = ''
  print("number points ",len(points))
  for p in points:
    ide, lat, lng, heading, pitch = p 
    img_name = str(ide) + '.jpg'
    gsv_query = get_gsv_query(lat, lng, heading, pitch, user_key)
    #print(gsv_query)
    download_single_gsv_image(gsv_query, download_folder, img_name)
  db_control.close_database()


############################################## PARALELLISM ##############################################


def multiprocessing_initial_requests_per_key(user_keys,requests_limit):

  # Setup a list of processes that we want to run  
  processes = []
  idx = 0
  for user_key in user_keys:
    #print('a-key: ', idx)
    not_processed_points = get_not_processed_points(requests_limit)
    if(len(not_processed_points)>0):
      block_not_processed_points(not_processed_points)
      processes.append( mp.Process(target=make_initial_google_requests, args=(not_processed_points, user_key)) )
      idx += 1
    else:
      break

  #print('len processes ', len(processes))
  # Run processes

  for p in processes:
    p.start()
    # Exit the completed processes
  for p in processes:
    p.join()


def multiprocessing_download_images_per_key(user_keys,requests_limit,download_folder):
  processes = []
  idx = 0
  for user_key in user_keys:
    #print('b-key: ', idx)
    not_download_points = get_unique_points_without_image(requests_limit)
    if(len(not_download_points)>0):
      block_not_download_image_points(not_download_points)
      processes.append( mp.Process(target=download_images, args=(not_download_points, download_folder, user_key)) )
      idx += 1
    else:
      break

  #print('len processes ', len(processes))
  # Run processes

  for p in processes:
    p.start()
    # Exit the completed processes
  for p in processes:
    p.join()



###############################################################################################

def make_initial_request_every_day(download_folder):
  
  schedule.every().day.at("11:05").do(complete_process,download_folder)
  while True:
    schedule.run_pending()
    time.sleep(1)

    #not_processed_points = get_not_processed_points(1)
    #not_processed_points_len = len(not_processed_points)
    #print('not_download_points_len ',not_processed_points_len)
    #if(not_processed_points_len==0):
    #  break 
   

def complete_process(download_folder, user_keys):
 
  free_quota_limit = 0  #should be 25000 /2
  number_requests = 0          
  requests_limit = 250 
  #2. Make initial google request to know the status of the query
  
  while(free_quota_limit>=number_requests):
    print('number of requests: ',number_requests)
    multiprocessing_initial_requests_per_key(user_keys,requests_limit)
    number_requests += requests_limit

  print('...look_for_repeated_responses..')  
  look_for_repeated_responses()
  print('...Starting download images ...') 
  
  free_quota_limit = 24500 
  number_requests = 0 
  
  while(free_quota_limit>=number_requests):
    print('number of download requests: ',number_requests)
    multiprocessing_download_images_per_key(user_keys, requests_limit, download_folder)
    number_requests += requests_limit

  print('...FINISH download images ...') 


###############################################################################################
def find_missing(download_folder):
  db_control = DB_Controller()
  points  = db_control.select_unique_points_with_image()
  print(len(points))
  for p in points:
    ide = p[0]
    filename = p[8]
    if not(os.path.exists(download_folder+'/'+filename)):
      db_control.update_image_info_to_neg(ide)
    #else:
    #  print('nothing')
  db_control.close_database()

###############################################################################################

if __name__ == "__main__":
  # measure time
  start_time = time.time()
  
  parser = argparse.ArgumentParser()  
  parser.add_argument('--input_file', type=str, default='input.txt')
  parser.add_argument('--output_folder', type=str, default=default_folder_name())
  args = parser.parse_args()

  filename = args.input_file
  download_folder = args.output_folder

  #1. Read the input file
  #read_file_insert_points(filename)
  #print(time.strftime("%Y-%m-%d"))
  #make_initial_request_every_day(download_folder)


  user_keys_1 = ['AIzaSyBELFMT-DH0AIcRv-_-8hClNd2mnYG-rkg',
                 'AIzaSyDB09RFL6NUVAOY9frsCxrPRxVNE2JdpoA',
                 'AIzaSyCdlHyjZnH5OVYxnoGXfh5TT8PB7xmwR2Q',
                 'AIzaSyBypQy8s9s2IwQ660K6U0QBt7Apg4KSnBM',
                 'AIzaSyBgzOkNeKfkY_B71zPOoRpwWsIVSYZhqoE']

  user_keys_2 = ['AIzaSyBC8Fw8R8dUJ_5DJ4A--sN0SFzglWHNgK8',
                 'AIzaSyB1RDpbyFi1eQCDvtMpp8BB3wVp2jhNCJk',
                 'AIzaSyDzzeo1GltAn9jSEBKW9TAIJiEfMLXU9AI',
                 'AIzaSyCP6nGWYf9nkfJmaXGEnGLti9elOpGFI44',
                 'AIzaSyBxPRNiCwKJDy2mqaPDBB90PHBuACI-Yfw',
                 'AIzaSyCBrL5QJgQye7JJnrxZ1JaNUgIl69P_HtU',
                 'AIzaSyAjQfayMt9HTuQ1t9x1Hk2hglGD_Qrf9Mg'] 
                
  
  complete_process(download_folder,user_keys_2)
  #complete_process(download_folder,user_keys_3)
  
  #download_images_with_invalid_copyright(download_folder)
  find_missing(download_folder)
  print("--- %s seconds ---" % (time.time() - start_time))