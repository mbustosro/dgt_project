
/*QUERIES TO CREATE TABLES*/
DROP TABLE IF EXISTS streetview_bcn.returned_pts;
DROP TABLE IF EXISTS streetview_bcn.search_pts;

CREATE TABLE streetview_bcn.search_pts (
	id integer PRIMARY KEY, 
	lat double precision NOT NULL, 
	lng double precision NOT NULL, 
	heading double precision NOT NULL, 
	pitch double precision NOT NULL DEFAULT 0, 
	processed_status character(1) NOT NULL DEFAULT 'N',
	response character varying
);

/*
Returned_pts table has the response from google street api
id: number of the point. The same from search_pts table
query_status:
-NOT PROCESSED: point just inserted. It just has the id. Other values are null
-READY: point already has a response by google street view api. It is ready to check if there are repeated points
-UNIQUE: point with a unique lat and lng. A gsv image has to be dowload for this point
-REPEATED: repeated point, there is an existing point with the same lat and lng. Repeated_id indicates what is the unique point
-ZERO_RESULTS: google street view api did not returned any image. 
-INVALID_COPYRIGHT: google street view returned an image but that is not owned by google
*/

CREATE TABLE streetview_bcn.returned_pts (
	id integer references streetview_bcn.search_pts(id) PRIMARY KEY, 
	query_status character varying NOT NULL DEFAULT 'NOT_PROCESSED', 
	lat double precision, 
	lng double precision, 
	heading double precision, 
	pitch double precision,
	repeated_id integer references streetview_bcn.search_pts(id), 
	download_status character(1) NOT NULL DEFAULT 'N', 
	gsv_img character varying,
	segmented_img character varying,
	blended_img character varying
);

/*ACCIDENTS*/
DROP TABLE IF EXISTS streetview_bcn.accidents_location;
CREATE TABLE streetview_bcn.accidents_location (
	exp_id character varying PRIMARY KEY,
	year integer,
	utm_x double precision,
	utm_y double precision,
	lat double precision,
	lng double precision
);

DROP TABLE IF EXISTS streetview_bcn.accidents_people;
CREATE TABLE streetview_bcn.accidents_people (
	exp_id character varying references streetview_bcn.accidents_location(exp_id),
	person_type character varying,
	age integer,
	genre character(1)
);

DROP TABLE IF EXISTS streetview_bcn.closest_points;
CREATE TABLE streetview_bcn.closest_points (
	exp_id character varying references streetview_bcn.accidents_location(exp_id),
	id integer references streetview_bcn.returned_pts(id),
	distance double precision
);

DROP TABLE IF EXISTS streetview_bcn.objects_image;
CREATE TABLE streetview_bcn.objects_image (
	id integer references streetview_bcn.search_pts(id) PRIMARY KEY, 
	road integer,
    sidewalk integer,
    building integer,
    wall integer,
    fence integer,
    pole integer,
    traffic_light integer,
    traffic_sign integer,
    vegetation integer,
    terrain integer,
    sky integer,
    person integer,
    rider integer,
    car integer,
    truck integer,
    bus integer,
    train integer,
    motorcycle integer,
    bicycle integer
);

DROP TABLE IF EXISTS streetview_bcn.coordinates;
CREATE TABLE streetview_bcn.coordinates (
	id integer references streetview_bcn.returned_pts(id) PRIMARY KEY, 
	lat double precision, 
	lng double precision, 
	heading double precision,
	cbk_lat double precision, 
	cbk_lng double precision, 
	original_cbk_lat double precision, 
	original_cbk_lng double precision
);

DROP TABLE IF EXISTS streetview_bcn.trams_openbcn;
CREATE TABLE streetview_bcn.trams_openbcn (
	id integer, 
	description character varying,
	order_idx integer, 
	lat double precision,
	lng double precision
);

DROP TABLE IF EXISTS streetview_bcn.tramos_trams_openbcn;
CREATE TABLE streetview_bcn.tramos_trams_openbcn (
	id_tramos integer, 
	id_trams_openbcn integer
);

DROP TABLE IF EXISTS streetview_bcn.traffic_flow;
CREATE TABLE streetview_bcn.traffic_flow (
	id_trams_openbcn integer, 
	year integer,
	month integer,
	weekday boolean,
	hour integer,
	flow integer,
	PRIMARY KEY (id_trams_openbcn, year, month, weekday, hour)
);

DROP TABLE IF EXISTS streetview_bcn.objects_visibility_3d;
CREATE TABLE streetview_bcn.objects_visibility_3d(
	id integer references streetview_bcn.returned_pts(id) PRIMARY KEY,
	road double precision, 
	building double precision, 
	sidewalk double precision, 
	sky double precision,
	road_sidewalk double precision
);

CREATE TABLE streetview_bcn.objects_3d(
	id integer references streetview_bcn.returned_pts(id) PRIMARY KEY,
	road double precision, 
	building double precision, 
	sidewalk double precision, 
	sky double precision
);

DROP TABLE IF EXISTS streetview_bcn.accidents_trams_openbcn;
CREATE TABLE streetview_bcn.accidents_trams_openbcn(
	exp_id character varying references streetview_bcn.accidents_location(exp_id) PRIMARY KEY,
	id_trams_openbcn integer
);

DROP TABLE IF EXISTS streetview_bcn.points_trams_openbcn;
CREATE TABLE streetview_bcn.points_trams_openbcn(
	id integer references streetview_bcn.returned_pts(id) PRIMARY KEY,
	id_trams_openbcn integer
);

DROP TABLE IF EXISTS streetview_bcn.accidents_around_point_200m;
CREATE TABLE streetview_bcn.accidents_around_point_200m(
	id integer references streetview_bcn.returned_pts(id),
	exp_id character varying references streetview_bcn.accidents_location(exp_id),
	distance double precision,
	PRIMARY KEY (id, exp_id)
);

DROP TABLE IF EXISTS streetview_bcn.neighbor_points;
CREATE TABLE streetview_bcn.neighbor_points(
	point_1 integer references streetview_bcn.returned_pts(id),
	point_2 integer references streetview_bcn.returned_pts(id),
	distance double precision,
	PRIMARY KEY (point_1, point_2)
);


DROP TABLE IF EXISTS streetview_bcn.neighbor_points;
CREATE TABLE streetview_bcn.neighbor_points(
	point_1 integer references streetview_bcn.returned_pts(id),
	point_2 integer references streetview_bcn.returned_pts(id),
	distance double precision,
	PRIMARY KEY (point_1, point_2)
);

DROP TABLE IF EXISTS streetview_bcn.bad_imgs;
CREATE TABLE streetview_bcn.bad_imgs(
	id integer references streetview_bcn.returned_pts(id) PRIMARY KEY
);


CREATE TABLE streetview_bcn.accidents_prob (
	id integer references streetview_bcn.returned_pts(id) PRIMARY KEY, 
	prob double precision, 
);

/*MADRID*/


DROP TABLE IF EXISTS streetview_madrid.returned_pts;
DROP TABLE IF EXISTS streetview_madrid.search_pts;

CREATE TABLE streetview_madrid.search_pts (
	id integer PRIMARY KEY, 
	lat double precision NOT NULL, 
	lng double precision NOT NULL, 
	heading double precision NOT NULL, 
	pitch double precision NOT NULL DEFAULT 0, 
	processed_status character(1) NOT NULL DEFAULT 'N',
	response character varying
);

/*
Returned_pts table has the response from google street api
id: number of the point. The same from search_pts table
query_status:
-NOT PROCESSED: point just inserted. It just has the id. Other values are null
-READY: point already has a response by google street view api. It is ready to check if there are repeated points
-UNIQUE: point with a unique lat and lng. A gsv image has to be dowload for this point
-REPEATED: repeated point, there is an existing point with the same lat and lng. Repeated_id indicates what is the unique point
-ZERO_RESULTS: google street view api did not returned any image. 
-INVALID_COPYRIGHT: google street view returned an image but that is not owned by google
*/

CREATE TABLE streetview_madrid.returned_pts (
	id integer references streetview_madrid.search_pts(id) PRIMARY KEY, 
	query_status character varying NOT NULL DEFAULT 'NOT_PROCESSED', 
	lat double precision, 
	lng double precision, 
	heading double precision, 
	pitch double precision,
	repeated_id integer references streetview_madrid.search_pts(id), 
	download_status character(1) NOT NULL DEFAULT 'N', 
	gsv_img character varying,
	segmented_img character varying,
	blended_img character varying
);

CREATE TABLE streetview_madrid.objects_3d(
	id integer references streetview_madrid.returned_pts(id) PRIMARY KEY,
	road double precision, 
	building double precision, 
	sidewalk double precision, 
	sky double precision
);

CREATE TABLE streetview_madrid.objects_image (
	id integer references streetview_madrid.search_pts(id) PRIMARY KEY, 
	road integer,
    sidewalk integer,
    building integer,
    wall integer,
    fence integer,
    pole integer,
    traffic_light integer,
    traffic_sign integer,
    vegetation integer,
    terrain integer,
    sky integer,
    person integer,
    rider integer,
    car integer,
    truck integer,
    bus integer,
    train integer,
    motorcycle integer,
    bicycle integer
);

CREATE TABLE streetview_madrid.neighbor_points(
	point_1 integer references streetview_madrid.returned_pts(id),
	point_2 integer references streetview_madrid.returned_pts(id),
	distance double precision,
	PRIMARY KEY (point_1, point_2)
);


CREATE TABLE streetview_madrid.accidents_50_prob (
	id integer references streetview_madrid.returned_pts(id) PRIMARY KEY, 
	prob double precision, 
);

DROP TABLE IF EXISTS streetview_madrid.accidents_around_point_200m;
CREATE TABLE streetview_madrid.accidents_around_point_200m(
	id integer references streetview_madrid.returned_pts(id),
	uid integer references streetview_madrid.accidents(uid),
	distance double precision
);