#LDA
import time
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing as mp

#connection to database
from postgres_connector import DB_connection
class DB_Controller:

	def __init__(self):
		self.dbname 	= 'espaiPersona' 
		#self.dbname 	= 'Dummy_dgt' 
		self.user		= 'postgres'
		self.host		= 'localhost'
		self.password	= '41392175xy'
		self.schema 	= 'streetview_madrid'
		#self.schema 	= 'dummy_schema'
		self.db_conn 	= DB_connection(self.dbname, self.user, self.host, self.password)

	def close_database(self):
		self.db_conn.close_connection()

	def get_accidents(self):
		query = ("SELECT DISTINCT al.exp_id, al.lat, al.lng  FROM "+ self.schema +".accidents_location AS al INNER JOIN "
				+ self.schema +".accidents_people AS ap ON al.exp_id=ap.exp_id WHERE ap.person_type = 'Vianant' ")
		rows = self.db_conn.execute_select(query)
		return rows

	def get_accidents_madrid(self):
		query = ("SELECT DISTINCT uid, lat, lng  FROM "+ self.schema +".accidents"
				+" WHERE lat!=0 and lat!=2 ")
		rows = self.db_conn.execute_select(query)
		return rows

	def get_point_tram(self, lat, lng):
		query = ("SELECT * FROM ( SELECT id,ST_contains(geom,ST_SetSRID(ST_MakePoint("+str(lng)+","+str(lat)+"),4326))"
				+" AS result FROM "+ self.schema +".buffer_tramos_openbcn) AS foo where result is TRUE LIMIT 1")
		rows = self.db_conn.execute_select(query)
		return rows

	def get_images_around(self, lat, lng):
		limit = 5
		query = ("SELECT AVG(road), AVG(sidewalk), AVG(building), AVG(wall), AVG(fence)," 
				+"AVG(pole), AVG(traffic_light), AVG(traffic_sign), "
				+"AVG(vegetation), AVG(terrain), AVG(sky), AVG(person), AVG(rider), AVG(car), "
				+"AVG(truck), AVG(bus), AVG(train), AVG(motorcycle), AVG(bicycle) FROM "
				+"(SELECT ST_Distance(ST_SetSRID(ST_MakePoint(lng, lat),4326), "
				            +"ST_SetSRID(ST_MakePoint("+str(lng)+", "+str(lat)+"),4326)) AS dist, oi.* "
							+"FROM "+ self.schema +".returned_pts as r "
							+"INNER JOIN "+ self.schema +".objects_image as oi ON r.id=oi.id "
							+"WHERE query_status='UNIQUE' "
							+"ORDER BY 1 LIMIT "+str(limit)+") AS foo ")
		rows = self.db_conn.execute_select(query)
		return rows

	def select_points_with_image(self):
		query = ("SELECT id, lat, lng FROM "+ self.schema +".returned_pts WHERE  query_status = 'UNIQUE' ORDER BY id")
		rows = self.db_conn.execute_select(query)
		return rows

	def select_points_with_image_greater_than(self,value):
		query = ("SELECT id, lat, lng FROM "+ self.schema +".returned_pts WHERE  query_status = 'UNIQUE' "
				+" AND id>"+str(value)+"ORDER BY id")
		rows = self.db_conn.execute_select(query)
		return rows

	def check_accidents_around(self,lat,lng,dist=500, min_dist=200):
		query = ("SELECT * FROM "
					+"(SELECT DISTINCT ST_Distance_sphere(ST_SetSRID(ST_MakePoint(lng, lat),4326), "
		            +"ST_SetSRID(ST_MakePoint("+str(lng)+", "+str(lat)+"),4326)) as res, al.exp_id, lat, lng "
					+"FROM "+ self.schema +".accidents_location AS al INNER JOIN "
					+self.schema +".accidents_people AS ap ON al.exp_id=ap.exp_id "
					+"WHERE ap.person_type = 'Vianant' ) AS foo "
					+"WHERE res<="+str(dist)+" AND res>"+str(min_dist))
		rows = self.db_conn.execute_select(query)
		return rows

	def check_accidents_around_madrid(self,lat,lng,dist=200):
		query = ("SELECT * FROM "
					+"(SELECT DISTINCT ST_Distance_sphere(ST_SetSRID(ST_MakePoint(lng, lat),4326), "
					+"ST_SetSRID(ST_MakePoint("+str(lng)+", "+str(lat)+"),4326)) as res, uid, lat, lng "
					+"FROM "+ self.schema +".accidents WHERE lat!=2 AND lat!=0) AS foo "
					+"WHERE res<="+str(dist)+"")
		rows = self.db_conn.execute_select(query)
		return rows

	def get_neighbors_points(self, ide, lat, lng):
		query = (" SELECT * FROM (SELECT id, ST_DistanceSphere(ST_SetSRID(ST_MakePoint(lng, lat),4326), "
				            +"ST_SetSRID(ST_MakePoint("+str(lng)+", "+str(lat)+"),4326)) AS dist "
							+"FROM "+ self.schema +".returned_pts "
							+"WHERE query_status='UNIQUE' and id != "+str(ide)+") as foo " 
							+"WHERE dist<300 ORDER BY dist")
		rows = self.db_conn.execute_select(query)
		return rows

	def insert_accidents_around_point_200m(self, ide, exp_id, res):
		query = ("INSERT INTO "+ self.schema +".accidents_around_point_200m (id, uid, distance) "
				+"VALUES ("+str(ide)+",'"+str(exp_id)+"',"+str(res)+");")
		self.db_conn.execute_query(query)

	def insert_accidents_trams_openbcn(self, exp_id, id_trams_openbcn):
		query = ("INSERT INTO "+ self.schema +".accidents_trams_openbcn (exp_id, id_trams_openbcn) "
				+"VALUES ('"+exp_id+"',"+str(id_trams_openbcn)+");")
		self.db_conn.execute_query(query)

	def insert_points_trams_openbcn(self, ide, id_trams_openbcn):
		query = ("INSERT INTO "+ self.schema +".points_trams_openbcn (id, id_trams_openbcn) "
				+"VALUES ('"+str(ide)+"',"+str(id_trams_openbcn)+");")
		self.db_conn.execute_query(query)

	def insert_neighbor_points(self,point_id_1,point_id_2,distance):
		query = ("INSERT INTO "+ self.schema +".neighbor_points (point_1, point_2, distance) "
				+"VALUES ("+str(point_id_1)+","+str(point_id_2)+","+str(distance)+");")
		self.db_conn.execute_query(query)


def insert_accidents_around_point_200m_by_chunk(points, process_idx):
	flag_mad = True
	
	i = 0
	for p in points:
		if(i%1000==0):
			print('processed '+str(i)+' points in chunk '+str(process_idx))
		ide, lat, lng = p
		db_control = DB_Controller()
		if(flag_mad):
			accidents = db_control.check_accidents_around_madrid(lat, lng)
		else:
			accidents = db_control.check_accidents_around(lat, lng)
		db_control.close_database()
		#print('found ',len(accidents))
		for a in accidents:
			res = a[0]
			exp_id = a[1] 
			#print(exp_id)
			try:
				db_control = DB_Controller()
				db_control.insert_accidents_around_point_200m(ide,exp_id,res)
				db_control.close_database()
			except: 
				continue
		#time.sleep(1)
		i += 1 
	

def insert_accidents_around_point_200m():
	db_control = DB_Controller()
	points = db_control.select_points_with_image()
	db_control.close_database()

	n_chunks = 10
	chunks = np.array_split(points,n_chunks)

	processes = []
	for i in range(n_chunks):
		processes.append(mp.Process(target=insert_accidents_around_point_200m_by_chunk, args=(chunks[i],i)))
	for p in processes:
		p.start()

	# Exit the completed processes
	for p in processes:
	    p.join()

	

def insert_accidents_trams_openbcn():
	db_control = DB_Controller()
	accident_points = db_control.get_accidents()
	i = 0
	for point in accident_points:
		if(i%1000==0):
			print(i)
		exp_id, lat, lng = point
		tram = db_control.get_point_tram(lat,lng)
		if tram:
			for t in tram:
				id_trams_openbcn = t[0]
				db_control.insert_accidents_trams_openbcn(exp_id,id_trams_openbcn)
		i += 1 
	db_control.close_database()


def insert_points_trams_openbcn():
	db_control = DB_Controller()
	points = db_control.select_points_with_image()
	i = 0
	for p in points:
		if(i%1000==0):
			print(i)
		ide, lat, lng = p
		tram = db_control.get_point_tram(lat,lng)
		if tram:
			for t in tram:
				id_trams_openbcn = t[0]
				db_control.insert_points_trams_openbcn(ide,id_trams_openbcn)
		i += 1 
	db_control.close_database()



def insert_neighbor_points():
	
	value = 1790894
	db_control = DB_Controller()
	points = db_control.select_points_with_image_greater_than(value)
	db_control.close_database()
	print(len(points))
	i = 0
	for p in points:
		if(i%1000==0):
			print(i)
		ide, lat, lng = p
		point_id_1 = ide
		db_control = DB_Controller()
		neighbors = db_control.get_neighbors_points(ide,lat,lng)
		for n in neighbors:
			point_id_2, dist = n
			
			db_control.insert_neighbor_points(point_id_1,point_id_2,dist)
			
		db_control.close_database()
		#print(neighbors)
		i += 1 
		#time.sleep(1)


def get_positives_accidents_not_in_trams():
	db_control = DB_Controller()
	accident_points = db_control.get_accidents()
	accidents_points_not_in_trams = []

	for point in accident_points:
		exp_id, lat, lng = point
		tram = db_control.get_point_tram(lat,lng)
		if not tram:
			accidents_points_not_in_trams.append([tram,exp_id,lat,lng])

	print(len(accidents_points_not_in_trams))		
	features = [None] * len(accidents_points_not_in_trams)
	f = None
	idx = 0
	for point in accidents_points_not_in_trams:
		tram,exp_id,lat,lng = point
		f = db_control.get_images_around(lat,lng)
		f = f[0]
		features[idx]= f
		idx += 1
		
	features = np.asarray(features)
	features = features.astype(float)
	return features


def get_negatives_accidents_not_in_trams():
	db_control = DB_Controller()
	points = db_control.select_points_with_image()
	n = 5500
	points_in_trams = [None] * n
	features = [None] * n
	f = None
	idx = 0
	for p in points:
		ide, lat, lng = p
		#checkear si esta en tram
		tram = db_control.get_point_tram(lat,lng)
		if not tram:
			#buscar si esta al menos alejada 100 metros de cualquier accidente
			accident = db_control.check_accidents_around(lat, lng)
			if not accident: 
				f = db_control.get_images_around(lat,lng)
				f = f[0]
				features[idx]= f
				idx += 1
				print(idx)
		if (idx==n):
			break
	features = np.asarray(features)
	features = features.astype(float)
	return features


def get_positives_traffic_flow():
	db_control = DB_Controller()
	accident_points = db_control.get_accidents()
	accidents_points_in_trams = []
	print(len(accident_points))
	for point in accident_points:
		exp_id, lat, lng = point
		tram = db_control.get_point_tram(lat,lng)
		if tram:
			accidents_points_in_trams.append([tram,exp_id,lat,lng])
	print(len(accidents_points_in_trams))

	features = [None] * len(accidents_points_in_trams)
	f = None
	idx = 0
	for point in accidents_points_in_trams:
		tram,exp_id,lat,lng = point
		f = db_control.get_images_around(lat,lng)
		f = f[0]
		features[idx]= f
		idx += 1
		
	features = np.asarray(features)
	features = features.astype(float)
	return features

def get_negatives_traffic_flow():
	db_control = DB_Controller()
	points = db_control.select_points_with_image()
	n = 4000
	points_in_trams = [None] * n
	features = [None] * n
	f = None
	idx = 0
	for p in points:
		ide, lat, lng = p
		#checkear si esta en tram
		tram = db_control.get_point_tram(lat,lng)
		if tram:
			#buscar si esta al menos alejada 100 metros de cualquier accidente
			accident = db_control.check_accidents_around(lat, lng)
			if not accident: 
				f = db_control.get_images_around(lat,lng)
				f = f[0]
				features[idx]= f
				idx += 1
				print(idx)
		if (idx==n):
			break
	features = np.asarray(features)
	features = features.astype(float)
	return features


if __name__ == "__main__":

	insert_accidents_around_point_200m()
	#insert_neighbor_points()
	#get accidents with images 
	#get points with no accidents around more close
	




