# Import google_streetview for the api module
# pip install google_streetview
import google_streetview_api as gsv_api
import numpy as np
import os
import json
import time

#global images
IMAGE_SIZE = '640x640' #maximum image size allowed by FREE google street view

############################################################################################################
################################### ******** UTILS ******** ###########################################

def default_folder_name():
  dir_path = os.path.dirname(os.path.abspath(__file__))
  today = time.strftime("%Y-%m-%d") # "%Y-%m-%d %H:%M:%S"
  return os.path.join(dir_path,'Downloads_'+ today) 

def read_file(filename):
  with open(filename) as f:
      file_content = f.readlines()
  # you may also want to remove whitespace characters like `\n` at the end of each line
  file_content = [x.strip() for x in file_content] 
  #file_content = file_content[1:] #skip first line that contains variable names
  return file_content

def get_folder_names(input_folder, total_points=600000):
  folder_names = []
  WIDTH = 20000
  for i in np.arange(0,total_points,WIDTH):
    if(i%WIDTH==0):
      folder_name = str(i)+'-'+str(i+WIDTH)
      folder_path = os.path.join(input_folder,folder_name)
      folder_names.append(folder_path)
  return folder_names
  
def output_file_heading():
  return 'id lat lng heading pitch query_status seen seen_id exact_lat exact_lng img_name'

def load_seen_images(filename):
  if os.path.exists(filename):
    seen = set()
    with open(filename) as f:
        file_content = f.readlines()
    file_content = [x.strip() for x in file_content] 
    file_content = file_content[1:] #remove heading line
    #print('file_content ')
    #print(file_content)
    for line in file_content:
      item = line.split(' ')
      ide, lat, lng, hdg, pitch, query_status = item[:6]
      if query_status == 'OK':
        was_seen = item[6]
        if was_seen == 'NEW':
          exact_lat, exact_lng = float(item[8]), float(item[9])
          seen.add(tuple((int(ide), exact_lat, exact_lng, float(hdg))))

    return seen
  else:
    output = open(filename,'a+')
    output.write(output_file_heading()) #write heading
    output.close()
    return set() 

### GOOGLE STREET VIEW API

def get_gsv_query(lat, lng, heading, pitch, user_key):
    lat = str(lat)
    lng = str(lng)
    heading = str(heading)
    pitch = str(pitch)
    return [{ 
              'size': IMAGE_SIZE, # max 640x640 pixels
              'location': lat+','+lng,
              'heading': heading,
              'pitch': pitch,
              'key': user_key
            }]

def make_google_street_view_queries(queries):
  results = gsv_api.results(queries)
  return results.do_metadata_requests()

def download_gsv_images(queries, metadata, download_folder):
  results = gsv_api.results(queries)
  results.set_metadata(metadata)
  path_metadata_json = os.path.join(download_folder,'metadata.json')
  results.save_metadata(path_metadata_json) # propiertes of the downloaded images
  results.download_images(download_folder, name_index=1)
  results.save_links(os.path.join(download_folder,'links.txt')) # each line contains the url of each image

def download_single_gsv_image(query, download_folder, img_name):
  results = gsv_api.results(query)
  results.download_single_image(download_folder, img_name)


################################### ******** UTILS ******** ###########################################
############################################################################################################