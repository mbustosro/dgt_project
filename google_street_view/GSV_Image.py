#global images
IMAGE_SIZE = '640x640' #maximum image size allowed by FREE google street view

############################################################################################################
################################### ******** OBJECT GSV ******** ###########################################

class GSV_Image:
  
  seen_id = -1
  status = 'NOT_PROCESSED'
  image_filename = ''
  image_path = ''

  def __init__(self, ide, lat, lng, heading, pitch):
    self.ide = ide
    self.lat = lat
    self.lng = lng
    self.heading = heading
    self.pitch = pitch

  def set_exact_location(self, exact_location):
    self.exact_lat = str(exact_location['lat'])
    self.exact_lng = str(exact_location['lng'])

  def set_status(self, status):
    self.status = status

  def get_gsv_query(self,user_key):
    return { 
              'size': IMAGE_SIZE, # max 640x640 pixels
              'location': self.lat+','+self.lng,
              'heading': self.heading,
              'pitch': self.pitch,
              'key': user_key
            }

  def get_new_gsv_query(self,user_key):
    return { 
              'size': IMAGE_SIZE, # max 640x640 pixels
              'location': self.exact_lat+','+self.exact_lng,
              'heading': self.heading,
              'pitch': self.pitch,
              'key': user_key
            }

  def get_exact_location_tuple(self):
    return tuple((int(self.ide), float(self.exact_lat), float(self.exact_lng), float(self.heading)))

  def get_exact_location_tuple_str(self):
    return tuple((self.ide, self.exact_lat, self.exact_lng, self.heading))

  def set_seen_id(self, seen_id):
    self.seen_id = seen_id

  def get_output_file_info(self, seen=False, img_name_idx=''):
    line = '\n' + self.ide + ' ' + self.lat + ' ' + self.lng + ' ' + self.heading + ' ' + self.pitch + ' '
    line += self.status 
    if self.status == 'OK':
      if seen:
        line += ' REPEATED ' + str(self.seen_id)
      else:
        line += ' NEW ' + str(self.seen_id) + ' ' + self.exact_lat + ' ' + self.exact_lng + ' ' + img_name_idx+'.jpg'
    
    return line 

################################### ******** OBJECT GSV ******** ###########################################
############################################################################################################
