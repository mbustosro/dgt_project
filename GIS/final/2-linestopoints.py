import geopandas as gpd
from shapely.geometry import mapping, shape, Point
import fiona
from fiona.crs import from_epsg
import sys

def pointsAlongLines(lines, increment):
    points = { idx:[] for idx in lines["uid"] }
    total = len(lines)
    for row in lines.itertuples(): 
        geometry = getattr(row,"geometry")
        idx = getattr(row,"uid")
        for geom in geometry:
            for pair in shape(geom).coords:
                points[idx].append(Point(pair))
        line_bounds = geometry.bounds
        line_length = geometry.length
        current_distance = 0
        while current_distance < line_length:
            pt = geometry.interpolate(current_distance)
            points[idx].append(pt)
            current_distance += increment
        total-=1
        if total % 100 == 0:
            print(str(total) + " remaining")
    return points


def saveLayer(points, crs, path, typ):
    #thisCrs = from_epsg(crs)
    #layer.to_file(output_path, driver='ESRI Shapefile')
    schema =  {
        'geometry': typ,
        'properties': {
            'uid': 'int',
            #'maxx': 'float',
            #'maxy': 'float',
            #'minx': 'float',
            #'miny': 'float',
        }
    }
    with fiona.open(path, 'w',crs=from_epsg(crs),driver='ESRI Shapefile', schema=schema) as output:
        total = len(points)
        for idx in points:
            for point in points[idx]:
                prop = {
                    'uid':idx, 
                    #"minx":point[1][0], 
                    #"miny":point[1][1], 
                    #"maxx":point[1][2], 
                    #"maxy":point[1][3]
                }
              # write the row (geometry + attributes in GeoJSON format)
                output.write({'geometry': mapping(point), 'properties':prop})
            total -= 1
            if total % 100 == 0:
                if total == 0:
                    print("Done")
                else:
                    print(str(total) + " remaining")

city = "rubi"
working_dir = "/Users/daniel/Documents/data/icgc/{0}/".format(city)
statics = {
    "crs": 25831,
    "input_path": sys.argv[3] + '/scratch/lines.shp',
    "output_path": sys.argv[3] + '/scratch/points.shp',
}

#print("Reading input data...")
lines = gpd.read_file(statics["input_path"])
print("\nGenerating points along lines")
points = pointsAlongLines(lines,1)
print("Saving layers")
saveLayer(points, statics["crs"], statics["output_path"], "Point")