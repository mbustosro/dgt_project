import geopandas as gpd
from shapely import ops
from shapely.geometry import mapping, shape, MultiLineString, LineString
from fiona.crs import from_epsg
import fiona
import itertools
import time
import matplotlib.pyplot as plt
import numpy as np
import os
import sys


def getCaseString(st):
    st_strip = st.rstrip()
    return "CAS NOT LIKE '%" + str(st_strip) + "'"


def getFilterCategories(file):
    with open(file) as f:
        filter_cats = [cas.rstrip()[:-1] for cas in f]
    return filter_cats


def filterLayer(layer, filter_cats):
    filtered_layer = layer[
        ~layer["CAS"].str.contains("|".join(filter_cats))
    ]
    return filtered_layer


def assignUniqueIds(layer, index):
    type_field = "CAS"
    sidewalk_type = "COM_17"
    unique_id = 1
    seen_ids = set()
    count = 0

    times = []
    time_averages = {
        ""
    }

    total_count = 0
    #u_ids = [0] * len(layer)
    u_ids = {}
    mainstart = time.time()
    thistime = time.time()
    print("Assigning ids...")
    for line in layer.itertuples():
        idx = line.Index
        if count>1000:
            total_count+=count
            print(len(layer)-len(seen_ids))
            times.append((time.time()-thistime)/count)
            thistime = time.time()
            count = 0
        #Only loop over lines that have not been assigned an ID
        if idx not in seen_ids:
            count+=1
            u_ids[unique_id] = []
            types_list = [getattr(line,type_field)]
            geometry = getattr(line,"geometry")

            possible_intersects_index = list(
                spatial_index.intersection(geometry.bounds)
            )
            possible_intersects = layer.iloc[
                possible_intersects_index
            ]

            #if line has no intersects with other lines,
            #assign the line a unique ID alone:
            if len(possible_intersects) == 0:
                if sidewalk_type in types_list[0]:
                    has_sidewalk = True
                    #u_ids[intersect_id] = unique_id
                    u_ids[unique_id].append(intersect_id)
                seen_ids.add(idx)

            #otherwise, find all of the lines that intersect this line
            #and all of THOSE lines' intersecting lines... etc...
            else:
                #intersectingLineList holds lines that have been
                #taken care of, while toAdd holds lines that need
                #to be taken care of. In the first step of the loop,
                #both hold just the initial Line
                intersecting_line_ids = set([idx])      
                thisBuff = geometry.buffer(0.01) 
                precise_matches = possible_intersects[
                    possible_intersects.intersects(thisBuff)
                ]
                remaining_matches = precise_matches[
                    ~precise_matches.index.isin(intersecting_line_ids)
                ]
                toAdd = remaining_matches.index.tolist()
                intersecting_line_ids = set([idx]+[i for i in toAdd])
                types_list += [layer.loc[i,type_field] for i in toAdd]
                while len(toAdd) > 0:
                    __toAdd = []
                    for lid in toAdd:
                        intersecting_geom = layer.loc[lid,"geometry"]
                        buff = intersecting_geom.buffer(0.01)
                        next_possible_intersect_index = [
                            int(i) for i in index.intersection(
                                intersecting_geom.bounds
                            )
                        ]
                        next_possible_intersects = layer.iloc[
                            next_possible_intersect_index
                        ]
                        precise_matches = next_possible_intersects[
                            next_possible_intersects.intersects(buff)
                        ]
                        remaining_matches = precise_matches[
                            ~precise_matches.index.isin(
                                intersecting_line_ids
                            )
                        ]
                        vals = remaining_matches.index.tolist()
                        for val in vals:
                            __toAdd.append(val)
                            intersecting_line_ids.add(val)
                            types_list.append(
                                remaining_matches.loc[
                                    val,type_field
                                ]
                            )
                            count+=1
                    toAdd = __toAdd
                has_sidewalk_list = [
                    typ for typ in types_list 
                    if sidewalk_type in typ
                ]
                has_sidewalk = len(has_sidewalk_list) > 0

                #intersecting_line_list holds all lines 
                #that make up one block
                #they will all take the same unique_id
                for intersect_id in intersecting_line_ids:
                    if has_sidewalk:
                        #u_ids[intersect_id] = unique_id
                        u_ids[unique_id].append(intersect_id)
                    seen_ids.add(intersect_id) 
            if has_sidewalk:
                unique_id += 1
            types_list = []
    print(time.time() - mainstart)
    return u_ids


def mergeLines(layer, u_ids):
    lines = {}
    multi_lines = {}
    for idx in u_ids:
        multi_line = MultiLineString([layer.loc[jdx,"geometry"] for jdx in u_ids[idx]])
        merged_line = ops.linemerge(multi_line)
        if merged_line.geom_type == "MultiLineString":
            first_pt = merged_line[0].coords[0]
            last_segment = merged_line[len(merged_line)-1]
            last_pt = last_segment.coords[len(last_segment.coords)-1]
            new_segment = LineString([first_pt,last_pt])
            combined = MultiLineString([line for line in merged_line].append(new_segment))
            second_merge = ops.linemerge(combined)
            if second_merge.geom_type == "LineString":
                lines[idx] = second_merge
            else:
                multi_lines[idx] = merged_line
        elif merged_line.geom_type == "LineString":
            lines[idx] = merged_line
    return lines, multi_lines


def saveLayer(lines, crs, path, typ):
    #thisCrs = from_epsg(crs)
    #layer.to_file(output_path, driver='ESRI Shapefile')
    schema =  {
        'geometry': typ,
        'properties': {'uid': 'int',}
    }
    with fiona.open(path, 'w',crs=from_epsg(crs),driver='ESRI Shapefile', schema=schema) as output:
        for idx in lines:
              prop = {'uid':idx}
              # write the row (geometry + attributes in GeoJSON format)
              output.write({'geometry': mapping(lines[idx]), 'properties':prop})


def pointsAlongLines(lines, increment):
    points = {}
    for idx in lines:
        line = lines[idx]
        line_length = line.length
        current_distance = 0
        counter = 0
        while current_distance < line_length:
            pt = line.interpolate(current_distance)
            new_idx = str(idx) + "_" + str(counter)
            points[new_idx] = pt
            current_distance += increment
            counter+=1
    return points


statics = {
    "crs": 25831,
    "input_path": sys.argv[1] + '/' + sys.argv[2],
    "output_path_polys": sys.argv[3] + '/scratch/polys.shp',
    "output_path_lines": sys.argv[3] + '/scratch/lines.shp',
    "filter_file": "/Users/daniel/Dropbox/casos.txt",
}


#"Reading input data...")
sidewalk_layer = gpd.read_file(statics["input_path"])

#"Filtering lines by type...")
filter_string = getFilterCategories(statics["filter_file"])
filtered_layer = filterLayer(sidewalk_layer, filter_string)
filtered_layer.reset_index(drop=True, inplace=True)
#print("Building spatial index...")
spatial_index = filtered_layer.sindex

print()
print("Assigning unique ids to lines...")
unique_ids = assignUniqueIds(filtered_layer, spatial_index)
merged_lines, multi_lines = mergeLines(filtered_layer, unique_ids)

polygons = ops.polygonize(list(merged_lines.values()))
polygons_with_id = {idx: poly for idx, poly in zip(merged_lines, polygons)}

print("Saving layers...")
saveLayer( multi_lines, statics["crs"], statics["output_path_lines"], "MultiLineString" )
saveLayer( polygons_with_id, statics["crs"], statics["output_path_polys"], "Polygon" )
