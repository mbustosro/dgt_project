from shapely import ops
from shapely import geometry
import geopandas as gpd
from scipy.spatial import Delaunay
import numpy as np
import math
import fiona
from fiona.crs import from_epsg
import matplotlib.pyplot as plt   
import time
import itertools
import sys

def makeHull(point_group, alpha):

    if len(point_group) < 4:
        # if the polygon is a triangle, return the triangle
        return geometry.MultiPoint(list(point_group)).convex_hull

    #make list of point coords for block
    coords = np.array(
        [
            getattr(row,"geometry").coords[0] 
            for row in point_group.itertuples()
        ]
    )

    #normalize the coordinates to improve triangulation
    mean = coords.mean(axis=0)
    coords -= mean

    #make triangulation
    triangulation = Delaunay(coords)
    vertices = triangulation.simplices
    triangles = [
        [ia, ib, ic] 
        for ia, ib, ic in vertices
    ]
    neighbors = {
        t: triangulation.neighbors[t] 
        for t in range(len(triangles))
    }

    deleted = set()
    #Get initial boundary triangles 
    #with the corresponding boundary edge
    #boundary_triangles is a dictionary 
    #where triangle_index: (pt_idx, pt_idx)
    boundary_triangles = {}
    for t in range(len(triangles)):
        for i in range(len(neighbors[t])):
            if neighbors[t][i] == -1:
                a, b = (
                    (0,1) if i == 2 
                    else (0,2) if i == 1 
                    else (1,2)
                )
                boundary = (
                    triangles[t][a], 
                    triangles[t][b]
                )
                boundary_triangles[t] = (
                    boundary[0],boundary[1]
                )
                break

    #begin loop to collapse polygons
    complete = False
    while not complete:
        complete = True
        next_edges = {}
        for i in boundary_triangles:
            #get the boundary edge of triangle
            boundary_edge = boundary_triangles[i]
            #measure length of edge
            pt_a = coords[boundary_edge[0]]
            pt_b = coords[boundary_edge[1]]
            dist = math.sqrt(
                (pt_a[0]-pt_b[0])**2 + (pt_a[1]-pt_b[1])**2
            )
            #if distance bigger than threshold (meters)
            if dist > alpha:
                deleted.add(i)
                complete = False
                #get neighbors of deleted triangle
                these_neighbors = [
                    n for n in neighbors[i] 
                    if n not in deleted and 
                        n not in boundary_triangles and 
                        n not in next_edges and
                        n != -1
                ]
                #set boundary_triangles 
                #to contain new boundary triangles
                for tr in these_neighbors:
                    for j in range(len(neighbors[tr])):
                        if neighbors[tr][j] == i:
                            a, b = (
                                (0,1) if j == 2 
                                else (0,2) if j == 1 
                                else (1,2)
                            )
                            boundary = (
                                triangles[tr][a], 
                                triangles[tr][b]
                            )
                            next_edges[tr] = (
                                boundary[0],boundary[1]
                            )
        boundary_triangles = next_edges

    edges = set()
    for t in range(len(triangles)):
        if t not in deleted:
            ia, ib, ic = triangles[t]
            for i, j in [(ia,ib),(ia,ic),(ib,ic)]:
                edges.add( (i, j) )
    edge_points = [coords[ [tup[0],tup[1]] ] for tup in edges]
    edge_points_after = [
        [pt+mean for pt in pts] for pts in edge_points
    ]
    m = geometry.MultiLineString(edge_points_after)
    triangles = list(ops.polygonize(m))
    union = ops.cascaded_union(triangles)
    return union


statics = {
    "crs": 25831,
    "input_path": sys.argv[3] + '/scratch/points.shp',
    "outputs": {
        "hulls": sys.argv[3] + '/scratch/hulls.shp',
        "triangles": sys.argv[3] + '/scratch/triangles'
    }
}

#print("Reading input data...")
points = gpd.read_file(statics["input_path"])

hulls = []
triangles = []
unique_ids = points["uid"].unique()
total = len(unique_ids)
print("\nBuilding hulls")
for idx in unique_ids:
    point_group = points[points["uid"] == idx]
    hull = makeHull ( point_group, alpha=5 )
    hulls.append(hull)
    total-=1
    if total % 100 == 0:
        if total == 0:
            print("Done")
        else:
            print(str(total) + " remaining")

print("\nSaving layers")
schema =  {
    'geometry': "Polygon",
    'properties': {'uid': 'int',}
}


with fiona.open(statics["outputs"]["hulls"], 'w',crs=from_epsg(statics["crs"]),driver='ESRI Shapefile', schema=schema) as output:
    for hull in hulls:
        prop = {'uid':0}
        # write the row (geometry + attributes in GeoJSON format)
        output.write({'geometry': geometry.mapping(hull), 'properties':prop})
