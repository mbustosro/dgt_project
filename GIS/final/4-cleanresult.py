import geopandas as gpd
from shapely import ops
from shapely.geometry import mapping
from fiona.crs import from_epsg
import fiona
import os
import sys


def mergeLayers(layer_1,layer_2):
    geoms = []
    for layer in (layer_1, layer_2):
        for poly in layer.itertuples():
            geom = getattr(poly,"geometry")
            if geom.is_valid:
                geoms.append(geom)
            else: 
                print("invalid")
    merged_layer = ops.cascaded_union(geoms)
    return merged_layer

def saveLayer(layer, output_path):
    schema =  {
        'geometry': "Polygon",
        'properties': {'uid': 'int',}
    }

    c=0
    with fiona.open(output_path, 'w',crs=from_epsg(25831),driver='ESRI Shapefile', schema=schema) as output:
        for poly in layer:
            prop = {'uid':c}
            output.write({'geometry': mapping(poly), 'properties':prop})
            c+=1

statics = {
    "input_paths": [
        sys.argv[3]+'/scratch/hulls.shp',
        sys.argv[3]+'/scratch/polys.shp',
        sys.argv[3]+'/scratch/blocks.shp'
    ],
    "output_path": sys.argv[3] + '/result.shp'
}

#print("Reading input data...")
print()
print("Cleaning final result")
layer_1 = gpd.read_file(statics["input_paths"][0])
layer_2 = gpd.read_file(statics["input_paths"][1])
blocks = gpd.read_file(statics["input_paths"][2])
merged_layer = mergeLayers(layer_1, layer_2)
final = merged_layer.difference(blocks)
print("\nSaving result...")
saveLayer(final, statics["output_path"])


