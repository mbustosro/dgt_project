function [border] = getPointEnvelope(points)
border = boundary(points(1,:)',points(2,:)',.95);
end