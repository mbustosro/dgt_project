import os
import sys
from qgis.core import * 
from qgis.gui import *
import qgis.utils
import toolkit
import matlab.engine

#path = "/Applications/QGIS.app/Contents/Resources/python/"
#os.chdir(path)

#Efficiently get a list of unique ids
#and a dictionary mapping features to
#unique ids
def getUniqueIdList(layer):
    u_id = "u_id"
    ids = []
    uniqueIdFeatureDict = {}
    for feature in layer.getFeatures():
        uid = str(feature[u_id])
        ids.append(feature[u_id])
        if uid in uniqueIdFeatureDict:
            uniqueIdFeatureDict[uid].append(feature)
        else: 
            uniqueIdFeatureDict[uid] = [feature]
    uniqueIds = list(set(ids))
    return uniqueIds, uniqueIdFeatureDict

#Generate and return QgsGeometry class from the 
#coordinates provided by Matlab Boundary() operation
def createPolygonFromHull(hull, points_list):
    xs = points_list[0]
    ys = points_list[1]
    polygon_points = []
    hullIntList = [int(ind[0]) for ind in hull]
    for index in hullIntList:
        x, y = xs[index-1], ys[index-1]
        point = QgsPoint(x, y)
        polygon_points.append(point)
    polygon = QgsGeometry.fromPolygon([polygon_points])
    return polygon

#Add new polygon geometry to Qgis layer
def addPolygonToPolyLayer(polygon):
    dataProvider = poly_layer.dataProvider()
    feature = QgsFeature()
    feature.setGeometry(polygon)
    dataProvider.addFeatures([feature])
    poly_layer.updateExtents()

input_path = sys.argv[1] + "/points/sidewalk_points.shp"
output_path = sys.argv[1] + "result/sidewalks.shp"

print("\n\nCode running...")

print("\n\nStarting QIS...")
app = QgsApplication([], True)
QgsApplication.initQgis()
print("QGIS started.\n\n")

print("Loading layers...")
print("Loading points...")
point_layer = toolkit.loadLayer(input_path, "points")
print("Loading polygons...")
poly_layer = toolkit.createMemoryLayer("Polygon", "polygons")
print("Layers loaded.\n\n")

print("Getting features by unique id...")
[uniqueIds, uniqueIdFeatureDict] = getUniqueIdList(point_layer)
print("Got features.\n\n")

print("Starting MATLAB...")
eng = matlab.engine.start_matlab()
print("MATLAB started.\n\n")

uniqueId_count = str(len(uniqueIds))
print("Starting loop...\n")
print("Number of unique ids: " + uniqueId_count)
x=0

#Loop through each unique id in the data set
for uid in uniqueIdFeatureDict:
    x += 1
    if x % 1000 == 0 and x != 0:
        print("Groups scanned:")
        print(str(x) + "\n")

    #produce two lists, point_xs[] and
    #point_ys[], that record the x and y 
    #coordinates of each point in the 
    #id group respectively. 
    #xs and ys will be matched by their index in 
    #point_xs[] and point_ys[]
    point_xs = []
    point_ys = []
    for point in uniqueIdFeatureDict[uid]:
        point_coords = point.geometry().asPoint()
        point_xs.append(point_coords[0])
        point_ys.append(point_coords[1])
    #lists are combined
    points_list = [point_xs, point_ys]
    #points_list generates matlab.double() obejct
    points_list_double = matlab.double(points_list)
    hull = eng.getPointEnvelope(points_list_double)
    polygon = createPolygonFromHull(hull, points_list)
    addPolygonToPolyLayer(polygon)

eng.quit()

print("\nInput point count: ")
print(point_layer.featureCount())
print("Polygons formed: ")
print(poly_layer.featureCount())


print("\nWriting layer")
toolkit.saveLayer(poly_layer, output_path)

QgsApplication.exitQgis()
