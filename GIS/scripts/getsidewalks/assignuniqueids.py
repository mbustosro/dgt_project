from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.gui import *
from qgis.utils import *
from PyQt4.QtCore import QVariant
from PyQt4.QtGui import QInputDialog
import toolkit
import itertools
import time


input_path = sys.argv[1]

output_path = (
    sys.argv[2]
)

sidewalk_layer_path = (
    input_path + "/sidewalks.shp"
)


#main function 
def assignUniqueIdsToSidewalks(sidewalk_layer):
    createUniqueIdField(sidewalk_layer)
    index = createSidewalkSpatialIndex(sidewalk_layer)
    sidewalk_intersections = findSidewalkIntersections(sidewalk_layer, index)
    return sidewalk_intersections

#Create a field called "u_id" to store unique ids
#if the field doesn't exist
def createUniqueIdField(layer):
    if layer.dataProvider().fieldNameIndex("u_id") == -1:
        layer.dataProvider().addAttributes(
            [QgsField("u_id", QVariant.Int)]
        )
        layer.updateFields()


#Generate spatial index on sidewalk lines layer
#to speed-up geometric searches
def createSidewalkSpatialIndex(sidewalkLayer):
    print("Generating spatial index...")
    index = QgsSpatialIndex()
    sidewalkLayerFeatures = sidewalkLayer.getFeatures()
    for feature in sidewalkLayerFeatures:
        index.insertFeature(feature)
    print("Index generated\n")
    return index


#Efficiently return a list of unique ids and 
#dictionary mapping ids to geometry features
def getUniqueIdList(layer):
    u_id = "u_id"
    ids = []
    uniqueIdFeatureDict = {}
    for feature in layer.getFeatures():
        uid = str(feature[u_id])
        ids.append(feature[u_id])
        if uid in uniqueIdFeatureDict:
            uniqueIdFeatureDict[uid].append(feature)
        else: 
            uniqueIdFeatureDict[uid] = [feature]
    uniqueIds = list(set(ids))
    return uniqueIds, uniqueIdFeatureDict


def findSidewalkIntersections(sidewalkLayer, index):
    typeField = "CAS"
    sidewalkType = "COM_17"
    print("Getting feature list...")
    sidewalks = {feature.id(): feature for (feature) in sidewalkLayer.getFeatures()}
    total_lines = len(sidewalks.values())
    print("Got feature list\n")
    dataProvider = sidewalkLayer.dataProvider()
    idIndex = dataProvider.fields().indexFromName("u_id")
    x = 0
    xx = 0
    uniqueId = 0

    print("Assigning ids...")

    updateMap = {}
    #lines will be removed from the list of sidewalks as they are
    #used, thus eventually the list variable sidewalks will reach length 0
    #indicating that all lines have been accounted for:
    start = time.time()
    last = 0
    secondsList = []
    while len(sidewalks.values()) > 0:
        line = sidewalks.values()[0]
        x += 1
        xx += 1
        if x % 1000 == 0:
            seconds = time.time() - start
            newLines = xx-last
            printStats(seconds, newLines, x, xx, total_lines)
            last = xx
            start = time.time()
            secondsList.append(newLines/seconds)

        #Only loop over lines that do not have an entry in updateMap{}
        #lines with entries have already been seen
        if line.id() not in updateMap:
            typesList = [line[typeField]]
            thisLineId = line.id()
            #get all lines that intersect this line
            thisLineIntersects = (
                index.intersects(line.geometry().boundingBox())
            )

            #if line has no intersects with other lines,
            #assign the line a unique ID alone:
            if len(thisLineIntersects) == 0:
                if sidewalkType in typesList[0]:
                    has_sidewalk = True
                    updateMap[thisLineId] = uniqueId
                    #updateMap[thisLineId] = {idIndex: uniqueId}
                sidewalks.pop(thisLineId, None)

            #otherwise, find all of the lines that intersect this line
            #and all of THOSE lines' intersecting lines... etc...
            else:
                #intersectingLineList holds lines that have been
                #taken care of, while toAdd holds lines that need
                #to be taken care of. In the first step of the loop,
                #both hold just the initial Line

                intersectingLineList = [thisLineId]
                toAdd = [thisLineId]
                while len(toAdd) > 0:
                    toAdd_temp = []
                    for lid in toAdd:
                        liIntersIds = index.intersects(
                            sidewalks[lid].geometry().boundingBox()
                        )
                        for intersectLineId in liIntersIds:
                            if intersectLineId in sidewalks:
                                if (
                                    sidewalks[intersectLineId].geometry()
                                    .intersects(sidewalks[lid].geometry())
                                ):
                                    if intersectLineId not in intersectingLineList:
                                        intersectingLineList.append(intersectLineId)
                                        typesList.append(sidewalks[intersectLineId][typeField])
                                        toAdd_temp.append(intersectLineId)
                    toAdd = toAdd_temp
                has_sidewalkList = [typ for typ in typesList if sidewalkType in typ]
                has_sidewalk = len(has_sidewalkList) > 0

                #
                for interLineId in intersectingLineList:
                    if has_sidewalk:
                        updateMap[interLineId] = uniqueId
                    sidewalks.pop(interLineId, None)
                    xx += 1
                xx -= 1
            if has_sidewalk:
                uniqueId += 1
            typesList = []
    return updateMap
    #dataProvider.changeAttributeValues(updateMap)


def printStats(seconds, newLines, groups, scanned, total):
    print("TIME:")
    print(seconds)
    print("")
    print("New lines scanned: " + str(newLines))
    print("\nLine groups scanned: " + str(groups))
    print("Total lines scanned: " + str(scanned))
    print("Lines remaining: " + str(total-scanned) + "\n")


def populateSidewalksIdLayer(layer, sidewalkLayer, data):
    layer_data_provider = layer.dataProvider()
    sidewalks = {feat.id(): feat for (feat) in sidewalkLayer.getFeatures()}
    feature_list = []
    for line_id in data:
        line = sidewalks[line_id]
        u_id = data[line_id]
        feature = QgsFeature()
        feature.initAttributes(1)
        feature.setGeometry(line.geometry())
        feature.setAttribute(0, u_id)
        feature_list.append(feature)
    layer_data_provider.addFeatures(feature_list)




sidewalk_layer = toolkit.loadLayer(sidewalk_layer_path, "sidewalks")
sidewalk_id_data = assignUniqueIdsToSidewalks(sidewalk_layer)
sidewalks_with_id = createMemoryLayer("LineString", "sidewalks_with_id")
sidewalks_with_id.dataProvider().addAttributes( 
            [QgsField("u_id", QVariant.Int)]
)
sidewalks_with_id.updateFields()
save_path = output_path + "/lines/sidewalks_with_id.shp"
toolkit.saveLayer(sidewalks_with_id, save_path)
#populateSidewalksIdLayer(sidewalks_with_id, sidewalkLayer, sidewalk_id_data)



#If lines don't have a u_id, it means they are not sidewalks
#This function deletes those lines
# def deleteNonSidewalkItems(sidewalkLayer):
#     print("Deleting unneeded lines")
#     deleteIds = []
#     for line in sidewalkLayer.getFeatures():
#         if not line["u_id"]:
#             deleteIds.append(line.id())
#     sidewalkLayer.dataProvider().deleteFeatures(deleteIds)
