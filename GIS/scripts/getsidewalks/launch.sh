#!/bin/bash
script_dir = $1
input_dir = $2
output_dir = $3
export PYTHONPATH=/Applications/QGIS.app/Contents/Resources/python/
export QGIS_PREFIX_PATH=/Applications/QGIS.app/Contents/MacOS/

python $script_dir/assignuniqueids.py $input_dir $output_dir

saga_cmd shapes_points 5 $output_dir/lines/sidewalks_with_id.shp $output_dir/points/sidewalk_points.shp

python $script_dir/createsidewalks.py $output_dir

