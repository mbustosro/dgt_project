from PyQt4.QtCore import *

def loadLayer(path, name):
    layer = QgsVectorLayer(path, name, 'ogr')
    QgsMapLayerRegistry.instance().addMapLayer(layer)
    return layer


def createMemoryLayer(typ, name):
	layer = QgsVectorLayer(typ, name, "memory")
	QgsMapLayerRegistry.instance().addMapLayer(layer)
	return layer


def createUniqueIdField(layer):
    if layer.dataProvider().fieldNameIndex("u_id") == -1:
        layer.dataProvider().addAttributes(
            [QgsField("u_id", QVariant.Int)]
        )
        layer.updateFields()


def setLayerUniqueId(layer):
	index = layer.fieldNameIndex("u_id")
	layer.startEditing()
	iterator = 0
	for feature in layer.getFeatures():
	    layer.changeAttributeValue(feature.id(), index, iterator)
	    iterator += 1
	layer.commitChanges()


def saveLayer(layer,path):
	error = QgsVectorFileWriter.writeAsVectorFormat(layer, path, "utf-8", None, "ESRI Shapefile", False)
	if error == QgsVectorFileWriter.NoError:
		print("Layer written successfully at: " +  path)
	else:
		print("ERROR: A layer was no exported successfully")


def createSpatialIndex(layer):
    print("Generating spatial index...")
    index = QgsSpatialIndex()
    layerFeatures = layer.getFeatures()
    for feature in layerFeatures:
        index.insertFeature(feature)
    print("Index generated\n")
    return index