#GIS Scripts

This directory stores the scripts used to manipulate and create geographic/geometric datasets for the project.

##Installation

##Dependencies
All dependencies listed here are general to all modules. See each module for a specific list of dependencies
###QGIS with GRASS 7
Written and tested with QGIS 2.18 Las Palmas
###SAGA GIS
Written and tested with SAGA 2.3.1

##Contents
###getsidewalks
Scripts to generate polygons for the sidewalks of an urban area from polyline data formatted according to the standards of the Institut Cartogr�fic i Geol�gic de Catalunya

###buildroadnetwork
Scripts to derive a list of points from a city street network, including the direction of traffic at each point along the network

###toolkit
A package of simple functions used regularly in many of the scripts in this directory. Usually, operations interacting with the QGIS API