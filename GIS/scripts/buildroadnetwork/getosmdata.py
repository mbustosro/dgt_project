import osmnx

working_dir = sys.argv[0]
folder = working_dir + "osm_streets"
city = sys.argv[1]
filename = city + "_drivable_streets"

print("Getting OSM data...")
G = osmnx.graph_from_place(city, network_type='drive')
print("Projecting data...")
G_projected = osmnx.project_graph(G)
print("Saving to shapefile...")
osmnx.save_graph_shapefile(G_projected, filename=filename, folder=folder)