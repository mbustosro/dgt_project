import json
import toolkit

json_path = "/Users/danielrhoads/Downloads/metadata.json"
data = json.load(open(json_path))

photo_points = toolkit.createMemoryLayer("Point", "photo points")
data_provider = photo_points.dataProvider()

points = []
for line in data:
	location = line["location"]
	y = location["lat"]
	x = location["lng"]
	feature = QgsFeature()
	print(str(y) + ", " + str(x))
	geometry = QgsGeometry.fromPoint(QgsPoint(x, y))
	feature.setGeometry(geometry)
	points.append(feature)
data_provider.addFeatures(points)
photo_points.updateExtents()