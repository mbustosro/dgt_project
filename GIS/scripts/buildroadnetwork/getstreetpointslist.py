import processing
import sys
import math
import toolkit
import random
from PyQt4.QtCore import *

#Paths to data
section_points_path = (
	sys.argv[0] + "/points/"
	"streetpoints.shp"
)
intersection_points_path = (
	sys.argv[0] + "/points/"
	"intersectpoints.shp"
)

#Measure the distance between two points
def distance(point_one,point_two):
    d = QgsDistanceArea()
    d.setEllipsoid('ETRS89')
    d.setEllipsoidalMode(True)
    distance = d.measureLine(
    	QgsPoint(point_one[0],point_one[1]), 
    	QgsPoint(point_two[0],point_two[1])
    )
    return distance

#Return the maximum number of streets 
#intersecting at a single point, from the data
def findMaxIntersections(layer):
	maximum = layer.fields().count()
	return maximum


#Create a dictionary where each key is 
#the unique ID of a section of street
#and each value is another dictionary 
#mapping point geometries to their order
#in the line 
#e.g. point_dictionary["10"]["0"] returns 
#the geometry of the first point (index 0) 
#of the line with unique id 10. 
def getPointDictionary(layer):
    point_dict = {}
    x = 0
    for point in layer.getFeatures():
    	x+=1
    	if x % 1000000 == 0:
    		print("\nRounds completed:")
    		print(x)
        position = str(point["PT_ID"])
        uid = str(point["u_id"])
        if uid in point_dict:
            point_dict[uid][position] = point
        else: 
            point_dict[uid] = {position: point}
    return point_dict


# #
# def getIntersectLineMap(layer):
# 	intersect_line_map = {}
# 	for feature in layer.getFeatures():
# 		intersect_line_map[feature] = []
# 		for field in layer.dataProvider().fields():
# 			name = field.name()
# 			if "id" in name and feature[name]:
# 				id_value = str(feature[name])
# 				intersect_line_map[feature].append(id_value)
# 	return intersect_line_map


#
def addIntersectsToPointDictionary(intersects):
	for intersect in intersects.getFeatures():
		intersect_geom = intersect.geometry()
		intersect_coords = intersect_geom.asPoint()
		intersect_rounded = (
			round(intersect_coords[0],6),round(intersect_coords[1],6)
		)
		intersect_id_list = (
			[
				intersect["id_"+str(n+1)] for n in range(max_intersections) 
				if intersect["id_"+str(n+1)]
			]
		)
		for uid in intersect_id_list:
			u_id = str(uid)
			line_length = len(point_dict[u_id])
			start_point = point_dict[u_id]["0"]
			end_point = point_dict[u_id][str(line_length-1)]
			start_point_coords = start_point.geometry().asPoint()
			start_point_rounded = (
				round(start_point_coords[0],6),round(start_point_coords[1],6)
			)
			end_point_coords = end_point.geometry().asPoint()
			end_point_rounded = (
				round(end_point_coords[0],6),round(end_point_coords[1],6)
			)
			if (
				intersect_rounded != start_point_rounded and 
				intersect_rounded != end_point_rounded
			):
				new_point = QgsFeature()
				new_point.setGeometry(intersect_geom)
				intersect_to_start_dist = distance(intersect_rounded, start_point_rounded)
				intersect_to_end_dist = distance(intersect_rounded, end_point_rounded)
				if intersect_to_start_dist < intersect_to_end_dist:
					for pos in reversed(range(line_length)):
						point_dict[u_id][str(pos+1)] = point_dict[u_id][str(pos)]
					point_dict[u_id]["0"] = new_point
				else:
					point_dict[u_id][str(line_length)] = new_point





def getSectionHeadingsMap(layer, point_dict, one_way_list):
	print("Finding point headings...")
	data_provider = layer.dataProvider()
	points_data_list = []
	#For each group of points
	line_count = len(point_dict)
	done_count = 0
	intersects_found = 0
	for unique_id in point_dict:
		if done_count % 10000 == 0:
			net_count = line_count - done_count
			print("\nLines scanned: " + str(done_count))
			print("Lines remaining: " + str(net_count))
			print("intersects found: " + str(intersects_found))
		one_way = unique_id in one_way_list
		last_point = point_dict[unique_id]["0"]
		section_length = len(point_dict[unique_id])
		section_range = range(section_length) 
		#for each point in the group (starting from first position)
		for position in section_range:
			heading_1 = None
			heading_2 = None
			point = point_dict[unique_id][str(position)]
			isFinalPoint = position == section_length - 1
			isFirstPoint = position == 0
			nextPointIsFinalPoint = position == section_length - 2
			previousPointIsFirstPoint = position == 1
			#if current point is not the last point
			if not isFinalPoint and not isFirstPoint:
				next_point = point_dict[unique_id][str(position+1)]
				heading_one_point_ahead = getAngleBetweenPoints(point, next_point)
				if not nextPointIsFinalPoint:
					two_points_ahead = point_dict[unique_id][str(position+2)]
					heading_two_points_ahead = getAngleBetweenPoints(point, two_points_ahead)
				else:
					heading_two_points_ahead = None
				if heading_two_points_ahead:
					raw_heading_1 = getAverageAngle(
							heading_one_point_ahead, heading_two_points_ahead
						)
					heading_1 = getPositiveAngle(raw_heading_1)
				else:
					heading_1 = getPositiveAngle(heading_one_point_ahead)
				if last_point and not one_way:
					heading_back_one_point = getAngleBetweenPoints(point, last_point)
					if not previousPointIsFirstPoint:
						two_points_back = point_dict[unique_id][str(position-2)]
						heading_back_two_points = getAngleBetweenPoints(point, two_points_back)
					else:
						heading_back_two_points = None
					if heading_back_two_points:
						raw_heading_2 = getAverageAngle(
							heading_back_one_point, heading_back_two_points
						)
						heading_2 = getPositiveAngle(raw_heading_2)
					else: 
						heading_2 = getPositiveAngle(heading_back_one_point)
				else:
					heading_2 = None
			point_coords = point.geometry().asPoint()
			lng = point_coords[0]
			lat = point_coords[1]
			point_data_1 = [lat, lng, heading_1]
			if heading_1:
				points_data_list.append(point_data_1)
			if heading_2:
				point_data_2 = [lat, lng, heading_2]
				points_data_list.append(point_data_2)
			last_point = point
		done_count += 1
	return points_data_list


# def findIntersect(uid, point, intersect_line_map):
# 	point_geometry = point.geometry().asPoint()
# 	point_geometry_rounded = (
# 		round(point_geometry[0], 6), round(point_geometry[1], 6)
# 	)
# 	intersect = None
# 	for inter in intersect_line_map:
# 		if uid in intersect_line_map[inter]:
# 			inter_geometry = inter.geometry().asPoint()
# 			inter_geometry_rounded = (
# 				round(inter_geometry[0], 6), round(inter_geometry[1], 6)
# 			)
# 			if inter_geometry_rounded == point_geometry_rounded:
# 				intersect = inter
# 	return intersect


# def handleIntersect(unique_id, intersect):
# 	intersect_data = getIntersectionHeadingData(unique_id, intersect)
# 	intersect_line_map.pop(intersect)
# 	return intersect_data


# def getIntersectionHeadingData(uid, intersect):

# 	intersect_field_names = []
# 	for i in range(max_intersections):
# 		intersect_field_name = "id_" + str(i+1) 
# 		intersect_field_names.append(intersect_field_name)

# 	inter_geometry = intersect.geometry().asPoint()
# 	headings_list = []
# 	lng = inter_geometry[0]
# 	lat = inter_geometry[1]

# 	intersects = [intersect[name] for name 
# 				  in intersect_field_names 
# 				  if intersect[name]]

# 	for uid in intersects:
# 		intersect_data = None
# 		line = point_dict[str(uid)]
# 		if len(line) != 1:
# 			next_point = findNextPointOnLine(line, inter_geometry)
# 			if next_point:
# 				heading = getAngleBetweenPoints(intersect, next_point)
# 			else:
# 				heading = None
# 			if heading:
# 				intersect_data = [lat, lng, heading]
# 			else: 
# 				intersect_data = None

# 	return intersect_data


## Test to see if car can enter street section from intersection
# def findNextPointOnLine(line, point_coords):
# 	oneway = False
# 	start_point = line["0"]
# 	start_point_coords = start_point.geometry().asPoint()
# 	start_rounded = (round(start_point_coords[0], 6), round(start_point_coords[1], 6))
# 	point_rounded = (round(point_coords[0], 6), round(point_coords[1], 6))
# 	if start_rounded == point_rounded:
# 		next_point = line["1"]
# 	else:
# 		if start_point["oneway"] == 1:
# 			oneway = True
# 		if oneway:
# 			next_point = None
# 		else:
# 			next_point_index = str(len(line)-2)
# 			next_point = line[next_point_index]
# 	return next_point


def getOneWayList(layer):
	ids = []
	for feature in layer.getFeatures():
		uid = str(feature["u_id"])
		if feature["oneway"] == 1:
			ids.append(uid)
	return ids


def getAngleBetweenPoints(origin, destination):
	origin_coords = origin.geometry().asPoint()
	destination_coords = destination.geometry().asPoint()
	origin_x = origin_coords[0]
	origin_y = origin_coords[1]
	destination_x = destination_coords[0]
	destination_y = destination_coords[1]
	delta_x = destination_x - origin_x
	delta_y = destination_y - origin_y
	angle_radians = math.atan2(delta_x, delta_y)
	angle_degrees = angle_radians / 0.017453
	return angle_degrees


def getPositiveAngle(angle):
	if angle < 0:
		positive_angle = 360 + angle
	else:
		positive_angle = angle
	return positive_angle


def getAverageAngle(angle_1,angle_2):
	average_angle = (angle_1 + angle_2) / 2
	return average_angle


def writeDataToFile(text_file_path, sections_map):
	f = open(
		text_file_path, "w+"
	)
	f.write("id lat lng heading pitch\r")
	identifier = 1
	#random.shuffle(sections_map)
	#random.shuffle(intersections_map)
	for view in sections_map:
		lat = round(view[0], 6)
		lng = round(view[1], 6)
		heading = round(view[2], 2)
		f.write("%s %s %s %s 0\r"%(identifier, lat, lng, heading))
		identifier += 1
	f.close()


##Algorithm

#Load layers
print("\nLoading layers...")
if not QgsMapLayerRegistry.instance().mapLayersByName("sections"):
	section_points = toolkit.loadLayer(section_points_path, "sections")
else:
	section_points = QgsMapLayerRegistry.instance().mapLayersByName("sections")[0]
if not QgsMapLayerRegistry.instance().mapLayersByName("intersections"):
	intersection_points = toolkit.loadLayer(intersection_points_path, "intersections")
else: 
	intersection_points = QgsMapLayerRegistry.instance().mapLayersByName("intersections")[0]
print("Layers loaded")

#Find max intersections
print("\nFinding max number of intersections")
max_intersections = findMaxIntersections(intersection_points)
print("Max number of intersections: " + str(max_intersections))

point_dict = getPointDictionary(section_points)
addIntersectsToPointDictionary(intersection_points)

one_way_list = getOneWayList(section_points)

#Find correct heading values for each point
#intersect_line_map = getIntersectLineMap(intersection_points)

print("\nGetting street section headings...")
sections_map = getSectionHeadingsMap(section_points, point_dict, one_way_list)
print("Got street section headings.")

#intersect_line_map = {}

print("\nGetting intersection point headings...")
#intersections_map = getIntersectionHeadingsMap(intersection_points, point_dict, max_intersections)
print("Got intersection point headings.")


print("\n")
print("\nWriting to file...")
text_file_path = (
	sys.argv[0] + "/txt/" + sys.argv[1] + ".txt"
)
writeDataToFile(text_file_path, sections_map)
print("Write complete.")


