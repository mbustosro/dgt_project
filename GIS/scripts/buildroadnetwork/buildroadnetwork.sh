#!/bin/bash
script_dir = $1
input_dir = $2
output_dir = $3
output_txt_name = $4
city = $5

export PYTHONPATH=/Applications/QGIS.app/Contents/Resources/python/
export QGIS_PREFIX_PATH=/Applications/QGIS.app/Contents/MacOS/

python $script_dir/getosmdata.py $input_dir $city

python $script_dir/buildnetwork.py $input_dir $output_dir $city

saga_cmd shapes_points 5 $output_dir/lines/streetlines.shp $output_dir/points/streetpoints.shp

python $script_dir/getstreetpointslist.py $output_dir $output_txt_name