import processing
import sys
from PyQt4.QtCore import *

exe_directory = sys.argv[1]
street_network_path = sys.argv[0] + "/osm_streets/" + sys.argv[2] + "_drivable_streets"


def addIdFields(layer, number):
	attributes_to_add_list = []
	for num in range(number):
		attribute_name = "id_" + str(num + 1)
		attribute = (
			QgsField(attribute_name, QVariant.Int)
		)
		attributes_to_add_list.append(attribute)
	layer.dataProvider().addAttributes(
		attributes_to_add_list
	)
	layer.updateFields()


def getLayerExtentString(layer):
	extent = layer.extent()
	xmin = extent.xMinimum()
	xmax = extent.xMaximum()
	ymin = extent.yMinimum()
	ymax = extent.yMaximum()
	extentString = (
		str(xmin) + ", " + str(xmax) + ", " + str(ymin) + ", " + str(ymax)
	)
	return extentString


def matchIdsOfIntersectingLinesToIntersections(layer, index):
	point_map = {}
	points = {feature.id(): feature for (feature) in layer.getFeatures()}
	total = layer.featureCount()
	count = 0
	for point in layer.getFeatures():
		if count % 1000 == 0:
			print("Points scanned:")
			print(count)
			print("\nPoints remaining:")
			print(total-count)
		point_geometry = point.geometry().asPoint()
		point_id = point["u_id"]
		if point_geometry not in point_map:
			point_map[point_geometry] = [point_id]
			intersecting_points = (
				index.intersects(point.geometry().boundingBox())
			)
			for other_point_ind in intersecting_points:
				other_point = points[other_point_ind]
				other_point_id = other_point["u_id"]
				other_point_geometry = other_point.geometry().asPoint()
				if (
					other_point_geometry == point_geometry
				):
					if other_point_id not in point_map[point_geometry]:
						point_map[point_geometry].append(other_point_id)
		count += 1
	return point_map


def findMaxIntersections(dictionary):
	list_list = []
	for key in dictionary:
		list_list.append(dictionary[key])
	longest = max(len(l) for l in list_list)
	return longest


def fillLayerWithPoints(layer, points_dict):
	data_provider = layer.dataProvider()
	#List to hold features
	points = []
	#List of id fields from memory layer
	fields = layer.fields()
	for point in points_dict:
		ids = points_dict[point]
		geometry = QgsGeometry.fromPoint(point)
		new_feature = QgsFeature()
		new_feature.setGeometry(geometry)
		new_feature.setFields(fields)
		for i in range(len(points_dict[point])):
			field_name = "id_" + str(i + 1)
			new_feature[field_name] = points_dict[point][i]
		points.append(new_feature)
	data_provider.addFeatures(points)
	layer.updateExtents()


def divideOneAndTwoWayStreets(layer):
	oneway_path = exe_directory + "/temp/raw/oneway/oneway.shp"
	twoway_path = exe_directory + "/temp/raw/oneway/twoway.shp"
	oneway_query = '"other_tags" LIKE \'%"oneway"=>"yes"%\''
	reverse_query = '"other_tags" NOT LIKE \'%"oneway"=>"yes"%\''
	oneway_layer = createMemoryLayer("Linestring", "oneway")
	oneway_dp = oneway_layer.dataProvider()
	twoway_layer = createMemoryLayer("Linestring", "twoway")
	twoway_dp = twoway_layer.dataProvider()
	print("\nselecting features")
	oneway_features = []
	twoway_features = []
	oneway_list = (
		layer.getFeatures(QgsFeatureRequest().setFilterExpression(oneway_query))
	)
	for feature in oneway_list:
		geometry = feature.geometry()
		newFeature = QgsFeature()
		newFeature.setGeometry(geometry)
		oneway_features.append(newFeature)
	oneway_dp.addFeatures(oneway_features)
	twoway_list = (
		layer.getFeatures(QgsFeatureRequest().setFilterExpression(reverse_query))
	)
	for oFeature in twoway_list:
		geometry = oFeature.geometry()
		newFeature = QgsFeature()
		newFeature.setGeometry(geometry)
		twoway_features.append(newFeature)
	twoway_dp.addFeatures(twoway_features)
	print("features selected")
	saveLayerFull(oneway_layer, oneway_path)
	saveLayerFull(twoway_layer, twoway_path)
	return oneway_layer, twoway_layer


def selectStreetsByClass(layer, oneWay):
	if oneWay:
		query = '"other_tags" LIKE \'%"oneway"=>"yes"%\''
	else:
		query = '"other_tags" NOT LIKE \'%"oneway"=>"yes"%\''
	feature_list = (
		layer.getFeatures(QgsFeatureRequest().setFilterExpression(query))
	)
	features = [feat.id() for feat in feature_list]
	layer.setSelectedFeatures(features)


def saveSelectedFeatures(layer, name):
	directory = exe_directory + "/temp/"
	path = directory + name + ".shp"
	features = layer.selectedFeatures()
	print(len(features))
	feature_list = []
	newLayer = createMemoryLayer("Linestring", name)
	for feature in features:
		feature_list.append(feature)
	newLayer.dataProvider().addFeatures(feature_list)
	saveLayerFull(newLayer, path)
	return path


def markOneWayStreets(layer, isOneWay):
	layer.startEditing()
	print("\ncreating one way field")
	createBoolField(layer, "oneway")
	print("field created.")
	features = layer.getFeatures()
	print("\nSetting oneway field...")
	if isOneWay:
		for feature in features:
			feature["oneway"] = 1
			layer.updateFeature(feature)
	else:
		for feature in features:
			feature["oneway"] = 0
			layer.updateFeature(feature)
	print("Field set.")
	layer.commitChanges()
	print("\nChanges committed")


def createBoolField(layer, name):
	field = QgsField(name, QVariant.Int)
	layer.dataProvider().addAttributes([field])
	layer.updateFields()


def removeUnneededFields(layer):
	print("\nRemoving unneeded fields...")
	fields = layer.dataProvider().fields()
	for i in range(len(fields)):
		if fields[i].name() != "oneway":
			layer.dataProvider().deleteAttributes([i])
	print("Removed.")
	layer.updateFields()


streets_layer = loadLayer(street_network_path, "streets")
extentString = getLayerExtentString(streets_layer)


#Break lines along vertices (QGIS)
print("\n1/12 - Exploding raw lines")
explode_return = processing.runalg("qgis:explodelines", streets_layer, None)
exploded_streets_path = explode_return["OUTPUT"]
exploded_streets = loadLayer(exploded_streets_path, "exploded")
print("Lines exploded...")


# #Break lines at intersections (GRASS)
# print("\n2/12 - Breaking streets...")
# break_return = (
# 	processing.runalg(
# 		"grass7:v.clean",
# 		exploded_streets, 			#Input data
# 		0, 							#operation: break
# 		0.1, 						#UNUSED VALUE
# 		extentString, 				#Default: use full extent of layer
# 		-1, 						#UNUSED VALUE
# 		.0001,						#UNUSED VALUE
# 		None,						#do not save output
# 		None						#do not save errors
# 	)
# )
# broken_streets_path = break_return["output"]
# broken_streets = loadLayer(broken_streets_path, "broken")
# print("Streets broken")


# #Remove duplicate lines (GRASS)
# print("\n3/12 - Removing duplicate streets...")
# clean_return = (
# 	processing.runalg(
# 		"grass7:v.clean",
# 		broken_streets, 			#Input data
# 		6, 							#operation: rmdpl
# 		0.1, 						#UNUSED VALUE
# 		extentString, 				#Default: use full extent of layer
# 		-1, 						#UNUSED VALUE
# 		0.0001,						#UNUSED VALUE
# 		None,						#do not save output
# 		None						#do not save errors
# 	)
# )
# clean_streets_path = clean_return["output"]
# clean_streets = loadLayer(clean_streets_path, "clean")
# print("Streets cleaned")


print("2/12 - extracting oneway streets")
selectStreetsByClass(exploded_streets, True)
one_way_path = saveSelectedFeatures(exploded_streets, "oneway_raw_saved")
one_way_streets = loadLayer(one_way_path, "oneway_raw")


print("3/12 - extracting twoway streets")
selectStreetsByClass(exploded_streets, False)
two_way_path = saveSelectedFeatures(exploded_streets, "twoway_raw_saved")
two_way_streets = loadLayer(two_way_path, "twoway_raw")


#Unify lines between intersections to create complete street sections (GRASS)
print("\n4/12 - Building twoway polylines...")
two_way_sections_path = exe_directory + "/temp/two_way/two_way.shp"
two_way_return = (
	processing.runalg(
		"grass7:v.build.polylines",
		two_way_streets, 			#Input data
		0, 							#Default: don't add info to lines
		extentString, 				#Default: use full extent of layer
		-1, 						#UNUSED VALUE
		0.0001, 					#UNUSED VALUE
		0,							#UNUSED VALUE
		two_way_sections_path
	)
)
two_way_sections = loadLayer(two_way_sections_path, "two_way")
print("Polylines built...")


print("\n5/12 - Building oneway polylines...")
one_way_sections_path = exe_directory + "/temp/one_way/one_way.shp"
one_way_return = (
	processing.runalg(
		"grass7:v.build.polylines",
		one_way_streets, 			#Input data
		0, 							#Default: don't add info to lines
		extentString, 				#Default: use full extent of layer
		-1, 						#UNUSED VALUE
		0.0001, 					#UNUSED VALUE
		0,							#UNUSED VALUE
		one_way_sections_path
	)
)
one_way_sections = loadLayer(one_way_sections_path, "one_way")
print("Polylines built...")

print("6/12 - Setting street attributes")
markOneWayStreets(one_way_sections, True)
markOneWayStreets(two_way_sections, False)

print("7/12 - Merging one and two way streets")
merge_return = processing.runalg(
	"qgis:mergevectorlayers",
	["one_way", "two_way"],
	None
)
merged_street_sections_path = merge_return["OUTPUT"]
merged_streets = loadLayer(merged_street_sections_path, "merged")
print("Streets merged")


#Break lines at intersections (GRASS)
print("\n8/12 - Breaking streets...")
break_return = (
	processing.runalg(
		"grass7:v.clean",
		merged_streets, 			#Input data
		0, 							#operation: break
		0.1, 						#UNUSED VALUE
		extentString, 				#Default: use full extent of layer
		-1, 						#UNUSED VALUE
		.0001,						#UNUSED VALUE
		None,						#do not save output
		None						#do not save errors
	)
)
broken_streets_path = break_return["output"]
broken_streets = loadLayer(broken_streets_path, "broken")
print("Streets broken")


#Remove duplicate lines (GRASS)
print("\n9/12 - Removing duplicate streets after breaking")
street_sections_file_name = "/sections/street_sections.shp"
street_sections_filepath = exe_directory + street_sections_file_name
clean_return = (
	processing.runalg(
		"grass7:v.clean",
		broken_streets, 			#Input data
		6, 							#operation: rmdpl
		0.1, 						#UNUSED VALUE
		extentString, 				#Default: use full extent of layer
		-1, 						#UNUSED VALUE
		0.0001,						#UNUSED VALUE
		street_sections_filepath,	#do not save output
		None						#do not save errors
	)
)
street_sections = loadLayer(street_sections_filepath, "clean")
print("Streets cleaned")


#Snap dangling vertices to create intersections between street sections (GRASS)
# print("\n9/12 - Snapping streets to intersections...")
# street_sections_file_name = "/sections/street_sections.shp"
# street_sections_filepath = exe_directory + street_sections_file_name
# snap_return = processing.runalg(
# 	"grass7:v.clean",
# 	merged_street_sections, 		#Input data
# 	1, 								#operation: snap
# 	0.1,
# 	extentString, 					#Default: use full extent of layer
# 	0.1, 							#UNUSED VALUE
# 	0.0001, 							#UNUSED VALUE
# 	street_sections_filepath, 		#Export street sections
# 	None
# )
# street_sections_path = snap_return["output"]
# street_sections = loadLayer(street_sections_path, "street_sections")
# print("Streets snapped")

print("\n10/12 - Generating unique ids for streets...")
createUniqueIdField(street_sections)
setLayerUniqueId(street_sections)
print("Ids generated")


#Extract intersections (as points) from street sections
print("\n11/12 - Extracting intersections as points...")
intersections_file_name = "/intersections/intersections_no_id.shp"
intersections_filepath = exe_directory + intersections_file_name
processing.runalg(
	"qgis:lineintersections",
	street_sections,
	street_sections,
	"u_id",
	"u_id",
	intersections_filepath
)
print("Intersections extracted")

print("12/12 - matching ids to intersections\n")
intersections = loadLayer(intersections_filepath, "intersections")
index = createSpatialIndex(intersections)

intersection_dict = matchIdsOfIntersectingLinesToIntersections(intersections, index)
max_intersections = findMaxIntersections(intersection_dict)

print("Creating new points layer...")
cleaned_intersections = createMemoryLayer("Point", "intersects")
print("Adding id fields")
addIdFields(cleaned_intersections, max_intersections)
print("Adding features...")
fillLayerWithPoints(cleaned_intersections, intersection_dict)

print("\nSaving layer")
cleaned_intersections_path = exe_directory + "/intersections/intersections.shp"
saveLayerFull(cleaned_intersections, cleaned_intersections_path)

print("\nAlgorithm complete.\n")